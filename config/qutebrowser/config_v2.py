# ---------------------------------------------------------
# Name:   qutebrowser config
# Author: Cyr4x3
# Descr.: My custom qutebrowser config.
# ---------------------------------------------------------


import subprocess
config.load_autoconfig()


# VARIABLES -------------------------------------------------------------

background = "#151515"
foreground = "#c4c4b5"
color0 = "#2b3135"
color1 = "#a66959"
color2 = "#769070"
color3 = "#ac8d6e"
color4 = "#607a86"
color5 = "#8a757e"
color6 = "#60867f"
color7 = "#1c1c1c"

startpage = '/home/ricard/Documents/Linux/Personalizacion/Firefox/Startpages/startpage/index.html'


# GENERAL ---------------------------------------------------------------

# Downloads
c.downloads.location.directory = '~/Downloads'
c.downloads.location.prompt = False

c.content.autoplay = False
c.auto_save.session = False

c.content.cookies.accept = 'no-3rdparty'
# c.content.tls.certificate_errors = 'load-insecurely'
# c.content.blocking.adblock.lists = ['https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/filters-2020.txt', 'https://easylist.to/easylist/easylist.txt', 'https://easylist.to/easylist/easyprivacy.txt','/home/hexdsl/.config/qutebrowser/lol.txt']
# c.content.blocking.method = 'both'

c.content.webgl = True
c.downloads.remove_finished = 5000
c.editor.command = ["kitty", "-e", "nvim", "{}"]

c.spellcheck.languages = ['en-US']


# KEYBINDINGS -----------------------------------------------------------

config.bind('<Alt+j>', 'tab-next')
config.bind('<Alt+k>', 'tab-prev')
config.bind('<Alt+l>', 'tab-give')
config.bind('<Alt-f>', 'hint inputs')
config.bind('<Ctrl-e>', 'open-editor')
config.bind('<Ctrl-h>', 'set-cmd-text :help :')
config.bind('<Tab>', 'set tabs.show always;; later 9000 set tabs.show switching')
config.bind('B', 'set-cmd-text -s :bookmark-load')
config.bind('E', 'config-edit')
config.bind('e', 'set-cmd-text -s :tab-select')
# config.bind('xb', 'set statusbar.show always;; later 5000 set statusbar.show never')
config.bind('xc', 'config-cycle tabs.show always never')
config.bind('xg', 'tab-give')
config.bind('xs', 'config-source')
config.bind('xx', 'set tabs.show always;; later 5000 set tabs.show switching')
config.bind('zd', 'download-open')

# Downloads stuff
config.bind(',d', 'set downloads.location.directory ~/Downloads/;; hint links download')
config.bind(',i', 'set downloads.location.directory ~/Pictures/;; hint images download')
# config.bind(',o', 'set downloads.location.directory ~/mnt/rust/obs/;; hint links download')
config.bind('<Ctrl-o>', 'prompt-open-download', mode='prompt')

# App shortcuts
# config.bind('<Alt-p>','spawn --userscript password_fill')
config.bind('<Ctrl-Shift-p>', 'spawn --userscript password_fill')
config.bind('<Ctrl-m>', 'spawn mpv --volume=50 {url}')
# config.bind('<Ctrl-r>', 'spawn --userscript readability')
config.bind('<Ctrl-y>', 'hint links spawn mpv --volume=50 {hint-url}')

# open custom pinned tabs
config.bind('xo','open https://www.youtube.com/dashboard;;tab-pin;;open -t https://hexdsl.co.uk;;tab-pin;;open -t http://localhost:8081/home/;;tab-pin;;open -t http://localhost:9091/transmission/web/#confirm;;tab-pin')

# Unbind some defaults
config.unbind('<Ctrl-v>')
config.unbind('<Ctrl-w>')
config.unbind('q')
# config.unbind('V')
config.unbind('v')
# config.unbind('z')


# UI --------------------------------------------------------------------

# c.content.fullscreen.window = True
# c.content.fullscreen.overlay_timeout = 0
c.content.geolocation = 'ask'
# c.content.notifications.enabled = False
# c.content.user_stylesheets = '~/.config/qutebrowser/lol.css'
c.messages.timeout = 5000
c.scrolling.bar = 'never'
c.scrolling.smooth = True
# c.statusbar.show = 'always'
c.zoom.default = '90%'

# tabs
c.tabs.background = True
c.tabs.favicons.scale = 1
c.tabs.indicator.padding = {"top": 0, "right": 0, "bottom": 0, "left": 0}
c.tabs.indicator.width = 0
c.tabs.padding = {"top": 2, "right": 2, "bottom": 2, "left": 2}
c.tabs.position = "left"
c.tabs.show = "switching"
c.tabs.title.format = '{index:>02}'
c.tabs.title.format_pinned = '{index:>02}'
c.tabs.width = 54

# urls
c.url.open_base_url = True
c.url.start_pages = startpage
c.url.default_page = startpage

# completion
c.completion.shrink = True
c.completion.scrollbar.width = 0
c.completion.open_categories = ['quickmarks', 'searchengines', 'history']

# search engine shortneners
c.url.searchengines = {
"DEFAULT": "https://www.google.co.uk/search?&q={}",
"goog": "https://www.google.co.uk/search?&q={}",
"googi": "https://www.google.co.uk/search?q={}&tbm=isch",
"wiki": "https://en.wikipedia.org/w/index.php?search={}",
"steam": "http://store.steampowered.com/search/?term={}",
"ddg": "https://duckduckgo.com/?q={}",
"aur": "https://aur.archlinux.org/packages/?O=0&K={}",
"aw": "https://wiki.archlinux.org/index.php?title=Special%3ASearch&search={}",
"gw": "https://wiki.gentoo.org/index.php?title=Special%3ASearch&search={}&go=Go",
"imdb": "http://www.imdb.com/find?ref_=nv_sr_fn&s=all&q={}",
"dic": "http://www.dictionary.com/browse/{}",
"ety": "http://www.etymonline.com/index.php?allowed_in_frame=0&search={}",
"urban": "http://www.urbandictionary.com/define.php?term={}",
"ddgi": "https://duckduckgo.com/?q={}&iar=images",
"lutris": "https://lutris.net/games/?q={}",
"deal": "https://isthereanydeal.com/search/?q={}",
"gog": "https://www.gog.com/games?sort=popularity&search={}&page=1",
"proton": "https://www.protondb.com/search?q={}",
"qwant": "https://www.qwant.com/?q={}",
"sp": "https://www.startpage.com/do/dsearch?query={}",
"humble": "https://www.humblebundle.com/store/search?sort=bestselling&search={}",
"tor": "https://www.magnetdl.com/search/?m=1&q={}",
"torrent": "https://www.magnetdl.com/search/?m=1&q={}",
"tom": "https://www.rottentomatoes.com/search?search={}",
"u": "http://www.urbandictionary.com/define.php?term={}",
"y": "https://www.youtube.com/results?search_query={}",
"r": "https://old.reddit.com/r/{}/new/",
"gt": "https://translate.google.com/#view=home&op=translate&sl=auto&tl=en&text={}",
"itch": "https://itch.io/search?q={}"}

# fonts
# c.fonts.contextmenu = 'Comic Sans MS'
# c.fonts.default_family = 'Comic Sans MS'
# c.fonts.default_size = "13pt"

# colors
# c.colors.webpage.preferred_color_scheme = 'dark'
c.colors.completion.category.bg = background
c.colors.completion.category.border.bottom = background
c.colors.completion.category.border.top = color7
c.colors.completion.category.border.top = color7
c.colors.completion.category.fg = foreground
c.colors.completion.category.fg = foreground
c.colors.completion.even.bg = color7
c.colors.completion.fg = foreground
c.colors.completion.item.selected.bg = foreground
c.colors.completion.item.selected.border.bottom = c.colors.completion.category.border.top
c.colors.completion.item.selected.border.top = c.colors.completion.item.selected.bg
c.colors.completion.item.selected.fg = background
c.colors.completion.match.fg = color2
c.colors.completion.odd.bg = background
# c.colors.contextmenu.disabled.bg = color7
# c.colors.contextmenu.disabled.fg = midgrey
# c.colors.contextmenu.menu.bg =  color7
# c.colors.contextmenu.menu.fg =  foreground
# c.colors.contextmenu.selected.bg = pink
# c.colors.contextmenu.selected.fg = foreground
c.colors.downloads.bar.bg = background
c.colors.downloads.error.bg = background
c.colors.downloads.error.fg = color1
c.colors.hints.bg = background
c.colors.hints.fg = color3
c.colors.hints.match.fg = foreground
c.colors.messages.info.bg = color4
c.colors.messages.info.fg = background
c.colors.messages.info.border = color4
c.colors.messages.warning.bg = color3
c.colors.messages.warning.fg = background
c.colors.messages.warning.border = color3
c.colors.messages.error.bg = color1
c.colors.messages.error.fg = foreground
c.colors.messages.error.border = color1
c.colors.statusbar.insert.bg = color2
c.colors.statusbar.url.success.https.fg = color2
c.colors.tabs.bar.bg = background
c.colors.tabs.even.bg = color7
c.colors.tabs.even.fg = c.colors.tabs.odd.fg
c.colors.tabs.odd.bg = background
c.colors.tabs.odd.fg = foreground
# c.colors.tabs.pinned.even.bg = background
# c.colors.tabs.pinned.even.fg = pink
# c.colors.tabs.pinned.odd.bg = midgrey
# c.colors.tabs.pinned.odd.fg = pink
# c.colors.tabs.pinned.selected.even.bg = pink
# c.colors.tabs.pinned.selected.odd.bg = pink
c.colors.tabs.selected.even.bg = foreground
c.colors.tabs.selected.odd.bg = foreground
c.hints.border = background
# c.colors.webpage.bg = midgrey
