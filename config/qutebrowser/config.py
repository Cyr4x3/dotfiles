# qutebrowser config.py
# cyr4x3 - (c) GNU GPLv3 2022
# custom configuration file for qutebrowser


import subprocess

# umatrix style blocking
# import sys, os
# sys.path.append(os.path.join(sys.path[0], 'jmatrix'))
# config.source("jmatrix/jmatrix/integrations/qutebrowser.py")

# -----------------------------------------------------------------------------
# General settings.
# -----------------------------------------------------------------------------

# sane defaults
c.backend = 'webengine'
c.auto_save.interval = 20000
c.auto_save.session = True
c.editor.command = ['urxvtc', '-e', 'nvim', '{}', "+call cursor({line}, {column})"]
#  c.spellcheck.languages = ['es-ES']
c.session.lazy_restore = True
c.confirm_quit = ['downloads']
c.input.escape_quits_reporter = True
c.input.forward_unbound_keys = 'auto'
c.content.geolocation = False
c.session.default_name = None

# c.window.title_format = '{current_title}'

# c.fileselect.handler = 'externa'
# c.fileselect.multiple_files.command = 'fff'
# c.fileselect.single_file.command = 'fff'

# -----------------------------------------------------------------------------
# Keybindings.
# -----------------------------------------------------------------------------

# custom binds
config.load_autoconfig()
c.aliases = {'h': 'help', 'q': 'close', 'x': 'quit --save'}

# defaults
config.bind('j', 'scroll-px 0 100')
config.bind('k', 'scroll-px 0 -100')
config.bind('b', 'set-cmd-text -s :tab-select ')
config.bind('return', 'selection-follow')
config.bind('cx', 'download-cancel')
config.bind('<Ctrl-s>', 'config-source ~/.config/qutebrowser/config.py')
config.bind('<Ctrl-E>', 'config-edit', mode='normal')

# mouse
#  c.input.mouse.rocker_gestures = True
#  c.input.mouse.back_forward_buttons = False

# tab management
config.bind('<Ctrl-k>', 'tab-move +')
config.bind('<Ctrl-j>', 'tab-move -')
config.bind('<Ctrl-e>', 'open-editor')
config.bind('<Ctrl-h>', 'set-cmd-text :help :')
config.bind('<Ctrl-o>', 'prompt-open-download', mode='prompt')
config.bind('<Alt+d>', 'tab-clone')
config.bind('<Alt+j>', 'tab-next')
config.bind('<Alt+k>', 'tab-prev')
config.bind('<Alt+l>', 'tab-give')
config.bind('<Alt-f>', 'hint inputs')

# quickmarks
config.bind('am', 'quickmark-load -t mail')
config.bind('ag', 'quickmark-load -t github')
config.bind('ar', 'quickmark-load -t reddit')
config.bind('ay', 'quickmark-load -t youtube')

# sessions
config.bind('e', 'set-cmd-text -s :session-load ')
config.bind('<Shift-e>', 'set-cmd-text -s :session-save -o ')

# downloads
config.bind('<Ctrl-d>', 'spawn qutedl {url}')
config.bind('<Ctrl-f>', 'spawn youtube-dl -x {url}')
config.bind('<Ctrl-m.', 'spawn mpv {url}')
config.bind(',d', 'set downloads.location.directory ~/Downloads/;; hint links download')
config.bind(',i', 'set downloads.location.directory ~/Pictures/;; hint images download')

# yank
config.bind("<y><o>", "yank inline [{title}]({url})")

# passthrough mode
config.bind('<Ctrl-V>', 'mode-enter passthrough')
config.bind('<Shift-Z>', 'mode-leave', mode='passthrough')

# insert mode
config.bind('<Escape>', 'mode-leave', mode='insert')
config.bind('<Shift-Ins>', 'insert-text {primary}', mode='insert')

config.bind('B', 'set-cmd-text -s :bookmark-load')
config.bind('E', 'config-edit')
config.bind('e', 'set-cmd-text -s :tab-select')
config.bind('xc', 'config-cycle tabs.show always never')
config.bind('xg', 'tab-give')
config.bind('xs', 'config-source')
config.bind('xx', 'set tabs.show always;; later 5000 set tabs.show switching')
config.bind('zd', 'download-open')

# Unbind some defaults
config.unbind('<Ctrl-v>')
config.unbind('<Ctrl-w>')
config.unbind('q')
config.unbind('v')
# config.unbind('V')
# config.unbind('z')

# insert
c.input.insert_mode.auto_load = False
c.input.insert_mode.auto_leave = True
c.input.insert_mode.auto_enter = True
c.input.insert_mode.leave_on_load = True
c.input.insert_mode.plugins = False

# -----------------------------------------------------------------------------
# UI
# -----------------------------------------------------------------------------

# hints
c.hints.mode = 'letter'
c.hints.chars = 'asdfhjkl'
c.hints.dictionary = '/usr/share/dict/words'
c.hints.min_chars = 1
c.hints.auto_follow = 'unique-match'
c.hints.auto_follow_timeout = 0
# Hide unmatched hints in rapid mode.
# c.hints.hide_unmatched_rapid_hints = True

# zoom
c.zoom.default = '90%'
c.zoom.levels = ['50%', '67%', '75%', '90%', '100%', '110%', '125%', '150%']
c.zoom.mouse_divider = 512

# statusbar
c.statusbar.padding = {'top': 5, 'bottom': 5, 'left': 5, 'right': 5}
c.statusbar.position = 'bottom'
c.statusbar.widgets = ['url', 'scroll']
#  c.statusbar.show = 'in-mode'

# completion
c.completion.show = 'always'
c.completion.delay = 0
c.completion.quick = True
c.completion.shrink = True
c.completion.height = '20%'
c.completion.min_chars = 1
c.completion.scrollbar.width = 0
c.completion.scrollbar.padding = 10
c.completion.use_best_match = False
c.completion.cmd_history_max_items = -1
c.completion.timestamp_format = '%d-%m-%Y-%H:%M:%S'
c.completion.open_categories = ['quickmarks', 'searchengines', 'history']

# tabs
# position
c.tabs.position = 'top'
c.tabs.width = 200
c.tabs.max_width = 0
c.tabs.min_width = 110
c.tabs.padding = {'top': 5, 'bottom': 5, 'left': 10, 'right': 10}
c.tabs.title.alignment = 'left'
c.tabs.title.format_pinned = '{index}'
c.tabs.title.format = '{audio}{current_title}'

# indicator
c.tabs.favicons.show = 'never'
c.tabs.indicator.width = 0
c.tabs.indicator.padding = {'top': 5, 'bottom': 10, 'left': 10, 'right': 10}

# behaviour
c.tabs.wrap = False
c.tabs.background = True
c.tabs.last_close = 'close'
c.tabs.close_mouse_button = 'right'
c.tabs.close_mouse_button_on_bar = 'new-tab'
c.tabs.mode_on_change = 'normal'
c.tabs.mousewheel_switching = True
c.tabs.new_position.stacking = True
c.tabs.new_position.related = 'next'
c.tabs.new_position.unrelated = 'next'
c.tabs.select_on_remove = 'next'
c.tabs.pinned.frozen = True
c.tabs.pinned.shrink = True
c.tabs.tabs_are_windows = False
c.tabs.show = 'always'
c.tabs.show_switching_delay = 1000
c.new_instance_open_target_window = 'last-focused'
# c.tabs.undo_stack_size = -1
# c.tabs.tooltips = True

# downloads
c.downloads.position = 'bottom'
c.downloads.remove_finished = -1
c.downloads.location.prompt = False
c.downloads.location.remember = True
c.downloads.location.suggestion = 'both'
c.downloads.location.directory = '~/Downloads'

# show a filebrowser in upload/download prompts.
c.prompt.filebrowser = True
c.downloads.open_dispatcher = None

# -----------------------------------------------------------------------------
# Privacy and Content
# -----------------------------------------------------------------------------

# YouTube AdBlock.
from qutebrowser.api import interceptor

def filter_yt(info: interceptor.Request):
    url = info.request_url
    if (
        url.host() == 'www.youtube.com'
        and url.path() == '/get_video_info'
        and '&adformat=' in url.query()
    ): info.block()

interceptor.register(filter_yt)

c.content.headers.user_agent = 'Mozilla/5.0 (Windows NT 6.1; rv:52.0) Gecko/20100101 Firefox/52.0'
c.content.headers.do_not_track = True
# c.content.host_blocking.enabled = True

# List of URLs of lists which contain hosts to block.  The file can be
# in one of the following formats:  - An `/etc/hosts`-like file - One
# host per line - A zip-file of any of the above, with either only one
# file, or a file   named `hosts` (with any extension).  It's also
# possible to add a local file or directory via a `file://` URL. In case
# of a directory, all files in the directory are read as dblock lists.
# The file `~/.config/qutebrowser/blocked-hosts` is always read if it
# c.content.host_blocking.lists = ['https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts']

# A list of patterns that should always be loaded, despite being ad-
# blocked. Note this whitelists blocked hosts, not first-party URLs. As
# an example, if `example.org` loads an ad from `ads.example.org`, the
# whitelisted host should be `ads.example.org`. If you want to disable
# the adblocker on a given page, use the `content.host_blocking.enabled`
# setting with a URL pattern instead. Local domains are always exempt
# from hostblocking.
# c.content.host_blocking.whitelist = ['googleadservices.com', 'adservice.google.com']

c.content.cookies.accept = 'no-3rdparty'
c.content.cookies.store = True
c.content.autoplay = False

# javascript
c.content.javascript.enabled = True
c.content.javascript.can_open_tabs_automatically = False
c.content.javascript.can_close_tabs = False
c.content.javascript.can_access_clipboard = False
c.content.javascript.alert = True
c.content.javascript.prompt = True

# fonts
c.fonts.web.size.minimum = 8
c.fonts.web.size.minimum_logical = 8
c.fonts.web.size.default = 12

# monospace fonts
c.fonts.hints = '8pt terminus'
c.fonts.keyhint = '8pt terminus'
c.fonts.prompts = '8pt terminus'
c.fonts.downloads = '8pt terminus'
c.fonts.statusbar = '8pt terminus'
c.fonts.messages.info = '8pt terminus'
c.fonts.debug_console = '8pt terminus'
c.fonts.messages.error = '8pt terminus'
c.fonts.completion.entry = '8pt terminus'
c.fonts.completion.category = '8pt terminus'
c.fonts.contextmenu = '8pt terminus'
c.fonts.tabs.selected = '8pt terminus'
c.fonts.tabs.unselected = '8pt terminus'

# c.fonts.web.family.cursive
# c.fonts.web.family.fantasy
c.fonts.web.family.fixed = 'Terminus (TTF)'
# c.fonts.web.family.sans_serif
# c.fonts.web.family.serif
# c.fonts.web.family.standard
# c.fonts.web.size.default
# c.fonts.web.size.default_fixed

# -----------------------------------------------------------------------------
# Colors.
# -----------------------------------------------------------------------------

# function to read colors from xresources
def read_xresources(prefix):
    props = {}
    x = subprocess.run(['xrdb', '-query'], stdout=subprocess.PIPE)
    lines = x.stdout.decode().split('\n')
    for line in filter(lambda l : l.startswith(prefix), lines):
        prop, _, value = line.partition(':\t')
        props[prop] = value
    return props
xresources = read_xresources('*')

# dark mode
c.colors.webpage.darkmode.enabled = True
c.colors.webpage.bg = xresources['*background']

# lightness-hsl brightness-rgb
c.colors.webpage.darkmode.algorithm = 'lightness-cielab'
c.colors.webpage.darkmode.grayscale.all = False
c.colors.webpage.darkmode.grayscale.images = 0

c.colors.webpage.prefers_color_scheme_dark = False

# always smart never
c.colors.webpage.darkmode.policy.page = 'always'
c.colors.webpage.darkmode.policy.images = 'never'

# 'smart'
# 0 - 256
# c.colors.webpage.darkmode.threshold.text = 256
# c.colors.webpage.darkmode.threshold.background = 0

# command bar
c.colors.statusbar.command.bg = xresources['*background']
c.colors.statusbar.command.fg = xresources['*color7']
c.colors.statusbar.command.private.bg = xresources['*background']
c.colors.statusbar.command.private.fg = xresources['*color7']

# status bar
c.colors.statusbar.normal.bg = xresources['*background']
c.colors.statusbar.normal.fg = xresources['*color7']
c.colors.statusbar.private.bg = xresources['*background']
c.colors.statusbar.private.fg = xresources['*color7']

# statusbar insert mode
c.colors.statusbar.insert.bg = xresources['*color7']
c.colors.statusbar.insert.fg = xresources['*background']

# default foreground color of the URL in the statusbar.
c.colors.statusbar.url.fg = xresources['*color7']
c.colors.statusbar.url.hover.fg = xresources['*color2']
c.colors.statusbar.url.success.http.fg = xresources['*color7']
c.colors.statusbar.url.success.https.fg = xresources['*color7']

# background color of the progress bar.
c.colors.statusbar.progress.bg = xresources['*background']

# foreground color of the URL in the statusbar when there's a warning.
c.colors.statusbar.url.warn.fg = xresources['*color1']
c.colors.statusbar.url.error.fg = xresources['*color2']

# hints
c.colors.hints.bg = xresources['*color7']
c.colors.hints.fg = xresources['*background']
c.colors.hints.match.fg = xresources['*color7']
c.hints.border = '1px solid' + str(xresources['*color7'])

# errors
c.colors.messages.error.bg = xresources['*color7']
c.colors.messages.error.fg = xresources['*background']
c.colors.downloads.error.bg = xresources['*color1']
c.colors.downloads.error.fg = xresources['*background']

# background color for the download bar.
c.colors.downloads.bar.bg = xresources['*background']
# color gradient start for download backgrounds.
c.colors.downloads.start.bg = xresources['*color2']
c.colors.downloads.start.fg = xresources['*background']
c.colors.downloads.stop.bg = xresources['*color5']
c.colors.downloads.stop.fg = xresources['*background']

# completion
c.colors.completion.category.bg = xresources['*color0']
c.colors.completion.category.fg = xresources['*foreground']
c.colors.completion.category.border.top = xresources['*color0']
c.colors.completion.category.border.bottom = xresources['*background']

# text color of the completion widget.
c.colors.completion.fg = xresources['*color7']
c.colors.completion.item.selected.bg = xresources['*color7']
c.colors.completion.item.selected.fg = xresources['*background']
c.colors.completion.item.selected.border.top = xresources['*background']
c.colors.completion.item.selected.border.bottom = xresources['*background']

# foreground color of the matched text in the completion.
c.colors.completion.match.fg = xresources['*color1']
c.colors.completion.odd.bg = xresources['*background']
c.colors.completion.even.bg = xresources['*background']
# color of the scrollbar in the completion view.
c.colors.completion.scrollbar.bg = xresources['*color6']
c.colors.completion.scrollbar.fg = xresources['*background']

# Background color of the keyhint widget.
# c.colors.keyhint.bg = 'rgba(0, 0, 0, 80%)'
# Text color for the keyhint widget.
# c.colors.keyhint.fg = '#FFFFFF'
# c.colors.keyhint.suffix.fg = '#FFFF00'

# Foreground color of an error message.
c.colors.messages.error.fg = xresources['*background']
c.colors.messages.error.border = xresources['*color1']

# Background color of an info message.
c.colors.messages.info.bg = xresources['*background']
c.colors.messages.info.border = xresources['*color7']
c.colors.messages.info.fg = xresources['*color7']

c.colors.messages.warning.bg = xresources['*color5']
c.colors.messages.warning.border = xresources['*color5']
c.colors.messages.warning.fg = xresources['*color1']

# Background color for prompts.
# c.colors.prompts.bg = '#444444'
# Border used around UI elements in prompts.
# c.colors.prompts.border = '1px solid gray'
# Foreground color for prompts.
# c.colors.prompts.fg = 'white'

# Background color for the selected item in filename prompts.
# c.colors.prompts.selected.bg = 'grey'

# Background color of the statusbar in caret mode.
# c.colors.statusbar.caret.bg = 'purple'

# Foreground color of the statusbar in caret mode.
# c.colors.statusbar.caret.fg = 'white'

# color of the statusbar in caret mode with a selection.
c.colors.statusbar.caret.selection.bg = xresources['*color7']
c.colors.statusbar.caret.selection.fg = xresources['*background']

c.colors.statusbar.passthrough.bg = xresources['*color7']
c.colors.statusbar.passthrough.fg = xresources['*background']

# tab
c.colors.tabs.bar.bg = xresources['*background']
c.colors.tabs.odd.fg = "#504945"
#  c.colors.tabs.odd.fg = xresources['*color7']
c.colors.tabs.odd.bg = xresources['*background']
c.colors.tabs.even.fg = "#504945"
#  c.colors.tabs.even.fg = xresources['*color7']
c.colors.tabs.even.bg = xresources['*background']
c.colors.tabs.selected.odd.bg = "#1F1F1F"
c.colors.tabs.selected.odd.fg = xresources['*color7']
#  c.colors.tabs.selected.odd.bg = xresources['*color7']
#  c.colors.tabs.selected.odd.fg = xresources['*background']
c.colors.tabs.selected.even.bg = "#1F1F1F"
c.colors.tabs.selected.even.fg = xresources['*color7']
#  c.colors.tabs.selected.even.bg = xresources['*color7']
#  c.colors.tabs.selected.even.fg = xresources['*background']
c.colors.tabs.indicator.error = xresources['*color1']

# right-click menu
c.colors.contextmenu.menu.bg = xresources['*background']
c.colors.contextmenu.menu.fg = xresources['*color7']
c.colors.contextmenu.selected.bg = xresources['*color7']
c.colors.contextmenu.selected.fg = xresources['*background']

## A list of patterns which should not be shown in the history. This only
## affects the completion. Matching URLs are still saved in the history
## (and visible on the qute://history page), but hidden in the
## completion. Changing this setting will cause the completion history to
## be regenerated on the next start, which will take a short while.
## Type: List of UrlPattern
# c.completion.web_history.exclude = []

## Number of URLs to show in the web history. 0: no history / -1:
## unlimited
## Type: Int
# c.completion.web_history.max_items = -1

## Enable support for the HTML 5 web application cache feature. An
## application cache acts like an HTTP cache in some sense. For documents
## that use the application cache via JavaScript, the loader engine will
## first ask the application cache for the contents, before hitting the
## network.
## Type: Bool
# c.content.cache.appcache = True

## Maximum number of pages to hold in the global memory page cache. The
## page cache allows for a nicer user experience when navigating forth or
## back to pages in the forward/back history, by pausing and resuming up
## to _n_ pages. For more information about the feature, please refer to:
## http://webkit.org/blog/427/webkit-page-cache-i-the-basics/
## Type: Int
# c.content.cache.maximum_pages = 0

## Size (in bytes) of the HTTP network cache. Null to use the default
## value. With QtWebEngine, the maximum supported value is 2147483647 (~2
## GB).
## Type: Int
# c.content.cache.size = None

## Allow websites to read canvas elements. Note this is needed for some
## websites to work properly.
## Type: Bool
# c.content.canvas_reading = True

## Default encoding to use for websites. The encoding must be a string
## describing an encoding such as _utf-8_, _iso-8859-1_, etc.
## Type: String
# c.content.default_encoding = 'iso-8859-1'

## Allow websites to share screen content. On Qt < 5.10, a dialog box is
## always displayed, even if this is set to "true".
## Type: BoolAsk
## Valid values:
##   - true
##   - false
##   - ask
# c.content.desktop_capture = 'ask'

## Try to pre-fetch DNS entries to speed up browsing.
## Type: Bool
# c.content.dns_prefetch = True

## Expand each subframe to its contents. This will flatten all the frames
## to become one scrollable page.
## Type: Bool
# c.content.frame_flattening = False


## Value to send in the `Accept-Language` header. Note that the value
## read from JavaScript is always the global value.
## Type: String
# c.content.headers.accept_language = 'en-US,en'

## Custom headers for qutebrowser HTTP requests.
## Type: Dict
# c.content.headers.custom = {}

## When to send the Referer header. The Referer header tells websites
## from which website you were coming from when visiting them. No restart
## is needed with QtWebKit.
## Type: String
## Valid values:
##   - always: Always send the Referer.
##   - never: Never send the Referer. This is not recommended, as some sites
##     may break.
##   - same-domain: Only send the Referer for the same domain. This will still
##     protect your privacy, but shouldn't break any sites. With QtWebEngine,
##     the referer will still be sent for other domains, but with stripped path
##     information.
# c.content.headers.referer = 'same-domain'

## Enable hyperlink auditing (`<a ping>`).
## Type: Bool
# c.content.hyperlink_auditing = False

## Load images automatically in web pages.
## Type: Bool
# c.content.images = True

## Log levels to use for JavaScript console logging messages. When a
## JavaScript message with the level given in the dictionary key is
## logged, the corresponding dictionary value selects the qutebrowser
## logger to use. On QtWebKit, the "unknown" setting is always used.
## Type: Dict
# c.content.javascript.log = {'unknown': 'debug', 'info': 'debug', 'warning': 'debug', 'error': 'debug'}

## Use the standard JavaScript modal dialog for `alert()` and
## `confirm()`.
## Type: Bool
# c.content.javascript.modal_dialog = False

## Allow locally loaded documents to access other local URLs.
## Type: Bool
# c.content.local_content_can_access_file_urls = True

## Allow locally loaded documents to access remote URLs.
## Type: Bool
# c.content.local_content_can_access_remote_urls = False

## Enable support for HTML 5 local storage and Web SQL.
## Type: Bool
# c.content.local_storage = True

## Allow websites to record audio/video.
## Type: BoolAsk
## Valid values:
##   - true
##   - false
##   - ask
# c.content.media_capture = 'ask'

## Allow websites to lock your mouse pointer.
## Type: BoolAsk
## Valid values:
##   - true
##   - false
##   - ask
c.content.mouse_lock = False

# Automatically mute tabs.
c.content.mute = False

## Netrc-file for HTTP authentication. If unset, `~/.netrc` is used.
## Type: File
# c.content.netrc_file = None

## Allow websites to show notifications.
## Type: BoolAsk
## Valid values:
##   - true
##   - false
##   - ask
c.content.notifications = False

## Allow pdf.js to view PDF files in the browser. Note that the files can
## still be downloaded by clicking the download button in the pdf.js
## viewer.
## Type: Bool
c.content.pdfjs = False

## Allow websites to request persistent storage quota via
## `navigator.webkitPersistentStorage.requestQuota`.
## Type: BoolAsk
## Valid values:
##   - true
##   - false
##   - ask
# c.content.persistent_storage = 'ask'

## Enable plugins in Web pages.
## Type: Bool
# c.content.plugins = False

## Draw the background color and images also when the page is printed.
## Type: Bool
# c.content.print_element_backgrounds = True

## Open new windows in private browsing mode which does not record
## visited pages.
## Type: Bool
# c.content.private_browsing = False

## Proxy to use. In addition to the listed values, you can use a
## `socks://...` or `http://...` URL.
## Type: Proxy
## Valid values:
##   - system: Use the system wide proxy.
##   - none: Don't use any proxy
# c.content.proxy = 'system'

## Send DNS requests over the configured proxy.
## Type: Bool
# c.content.proxy_dns_requests = True

## Allow websites to register protocol handlers via
## `navigator.registerProtocolHandler`.
## Type: BoolAsk
## Valid values:
##   - true
##   - false
##   - ask
# c.content.register_protocol_handler = 'ask'

## Validate SSL handshakes.
## Type: BoolAsk
## Valid values:
##   - true
##   - false
##   - ask
# c.content.ssl_strict = 'ask'

## List of user stylesheet filenames to use.
## Type: List of File, or File
# c.content.user_stylesheets = []

## Enable WebGL.
## Type: Bool
# c.content.webgl = True

## limit fullscreen to the browser window (does not expand to fill the screen)
# c.content.windowed_fullscreen = False

## Monitor load requests for cross-site scripting attempts. Suspicious
## scripts will be blocked and reported in the inspector's JavaScript
## console.
## Type: Bool
# c.content.xss_auditing = True

## Comma-separated list of regular expressions to use for 'next' links.
## Type: List of Regex
# c.hints.next_regexes = ['\\bnext\\b', '\\bmore\\b', '\\bnewer\\b', '\\b[>→≫]\\b', '\\b(>>|»)\\b', '\\bcontinue\\b']

## Comma-separated list of regular expressions to use for 'prev' links.
## Type: List of Regex
# c.hints.prev_regexes = ['\\bprev(ious)?\\b', '\\bback\\b', '\\bolder\\b', '\\b[<←≪]\\b', '\\b(<<|«)\\b']

## Scatter hint key chains (like Vimium) or not (like dwb). Ignored for
## number hints.
# c.hints.scatter = True

## CSS selectors used to determine which elements on a page should have
## hints.
## Type: Dict
# c.hints.selectors = {'all': ['a', 'area', 'textarea', 'select', 'input:not([type="hidden"])', 'button', 'frame', 'iframe', 'img', 'link', 'summary', '[onclick]', '[onmousedown]', '[role="link"]', '[role="option"]', '[role="button"]', '[ng-click]', '[ngClick]', '[data-ng-click]', '[x-ng-click]', '[tabindex]'], 'links': ['a[href]', 'area[href]', 'link[href]', '[role="link"][href]'], 'images': ['img'], 'media': ['audio', 'img', 'video'], 'url': ['[src]', '[href]'], 'inputs': ['input[type="text"]', 'input[type="date"]', 'input[type="datetime-local"]', 'input[type="email"]', 'input[type="month"]', 'input[type="number"]', 'input[type="password"]', 'input[type="search"]', 'input[type="tel"]', 'input[type="time"]', 'input[type="url"]', 'input[type="week"]', 'input:not([type])', 'textarea']}

## Maximum time (in minutes) between two history items for them to be
## considered being from the same browsing session. Items with less time
## between them are grouped when being displayed in `:history`. Use -1 to
## disable separation.
## Type: Int
# c.history_gap_interval = 30

## Include hyperlinks in the keyboard focus chain when tabbing.
## Type: Bool
# c.input.links_included_in_focus_chain = True

## Timeout (in milliseconds) for partially typed key bindings. If the
## current input forms only partial matches, the keystring will be
## cleared after this time.
## Type: Int
# c.input.partial_timeout = 5000

## Enable spatial navigation. Spatial navigation consists in the ability
## to navigate between focusable elements in a Web page, such as
## hyperlinks and form controls, by using Left, Right, Up and Down arrow
## keys. For example, if the user presses the Right key, heuristics
## determine whether there is an element he might be trying to reach
## towards the right and which element he probably wants.
## Type: Bool
# c.input.spatial_navigation = False

## Keychains that shouldn't be shown in the keyhint dialog. Globs are
## supported, so `;*` will blacklist all keychains starting with `;`. Use
## `*` to disable keyhints.
## Type: List of String
# c.keyhint.blacklist = []

## Time (in milliseconds) from pressing a key to seeing the keyhint
## dialog.
## Type: Int
# c.keyhint.delay = 500

## Rounding radius (in pixels) for the edges of the keyhint dialog.
## Type: Int
# c.keyhint.radius = 6

## Duration (in milliseconds) to show messages in the statusbar for. Set
## to 0 to never clear messages.
## Type: Int
c.messages.timeout = 3000

##   - tab: Open a new tab in the existing window and activate the window.
##   - tab-bg: Open a new background tab in the existing window and activate
##     the window.
##   - tab-silent: Open a new tab in the existing window without activating the
##     window.
##   - tab-bg-silent: Open a new background tab in the existing window without
##     activating the window.
##   - window: Open in a new window.
c.new_instance_open_target = 'tab'

# Rounding radius (in pixels) for the edges of prompts.
c.prompt.radius = 20

## Additional arguments to pass to Qt, without leading `--`. With
## QtWebEngine, some Chromium arguments (see
## https://peter.sh/experiments/chromium-command-line-switches/ for a
## list) will work.
## Type: List of String
# c.qt.args = ["blink-settings=darkMode=4"]

## Force a Qt platform to use. This sets the `QT_QPA_PLATFORM`
## environment variable and is useful to force using the XCB plugin when
## running QtWebEngine on Wayland.
## Type: String
# c.qt.force_platform = None

## Force software rendering for QtWebEngine. This is needed for
## QtWebEngine to work with Nouveau drivers and can be useful in other
## scenarios related to graphic issues.
## Type: String
## Valid values:
##   - software-opengl: Tell LibGL to use a software implementation of GL (`LIBGL_ALWAYS_SOFTWARE` / `QT_XCB_FORCE_SOFTWARE_OPENGL`)
##   - qt-quick: Tell Qt Quick to use a software renderer instead of OpenGL. (`QT_QUICK_BACKEND=software`)
##   - chromium: Tell Chromium to disable GPU support and use Skia software rendering instead. (`--disable-gpu`)
##   - none: Don't force software rendering.
# c.qt.force_software_rendering = 'none'

## Turn on Qt HighDPI scaling. This is equivalent to setting
## QT_AUTO_SCREEN_SCALE_FACTOR=1 in the environment. It's off by default
## as it can cause issues with some bitmap fonts. As an alternative to
## this, it's possible to set font sizes and the `zoom.default` setting.
## Type: Bool
# c.qt.highdpi = False

## When to use Chromium's low-end device mode. This improves the RAM
## usage of renderer processes, at the expense of performance.
## Type: String
## Valid values:
##   - always: Always use low-end device mode.
##   - auto: Decide automatically (uses low-end mode with < 1 GB available RAM).
##   - never: Never use low-end device mode.
# c.qt.low_end_device_mode = 'auto'

## Which Chromium process model to use. Alternative process models use
## less resources, but decrease security and robustness. See the
## following pages for more details:    -
## https://www.chromium.org/developers/design-documents/process-models
## - https://doc.qt.io/qt-5/qtwebengine-features.html#process-models
## Type: String
## Valid values:

##   - process-per-site-instance: Pages from separate sites are put into
##     separate processes and separate visits to the same site are also
##     isolated.
##   - process-per-site: Pages from separate sites are put into separate
##     processes. Unlike Process per Site Instance, all visits to the same site
##     will share an OS process. The benefit of this model is reduced memory
##     consumption, because more web pages will share processes. The drawbacks
##     include reduced security, robustness, and responsiveness.
##   - single-process: Run all tabs in a single process. This should be used
##     for debugging purposes only, and it disables `:open --private`.
# c.qt.process_model = 'process-per-site-instance'

    # scroll + search
c.scrolling.bar = 'when-searching'
c.scrolling.smooth = False
c.search.ignore_case = 'smart'
c.search.incremental = True

# -----------------------------------------------------------------------------
# URLs
# -----------------------------------------------------------------------------

# URL parameters to strip with `:yank url`.
# Type: List of String
# c.url.yank_ignored_parameters = ['ref', 'utm_source', 'utm_medium', 'utm_campaign', 'utm_term', 'utm_content']

# start
startpage = '~/.dotfiles/config/startpages/1/index.html'
c.url.start_pages = startpage
c.url.default_page = startpage

## What search to start when something else than a URL is entered.
## Type: String
## Valid values:
##   - naive: Use simple/naive check.
##   - dns: Use DNS requests (might be slow!).
##   - never: Never search automatically.
# c.url.auto_search = 'naive'


## URL segments where `:navigate increment/decrement` will search for a
## number.
## Type: FlagList
## Valid values:
##   - host
##   - port
##   - path
##   - query
##   - anchor
# c.url.incdec_segments = ['path', 'query']
c.url.open_base_url = True

# search engine shortneners
c.url.searchengines = {
"DEFAULT": "https://duckduckgo.com/?q={}",
"goog": "https://www.google.com/search?&q={}",
"googi": "https://www.google.com/search?q={}&tbm=isch",
"wiki": "https://en.wikipedia.org/w/index.php?search={}",
"steam": "http://store.steampowered.com/search/?term={}",
"ddg": "https://duckduckgo.com/?q={}",
"aur": "https://aur.archlinux.org/packages/?O=0&K={}",
"aw": "https://wiki.archlinux.org/index.php?title=Special%3ASearch&search={}",
"gw": "https://wiki.gentoo.org/index.php?title=Special%3ASearch&search={}&go=Go",
"imdb": "http://www.imdb.com/find?ref_=nv_sr_fn&s=all&q={}",
"inv": "https://yewtu.be/search?q={}",
"dic": "http://www.dictionary.com/browse/{}",
"ety": "http://www.etymonline.com/index.php?allowed_in_frame=0&search={}",
"urban": "http://www.urbandictionary.com/define.php?term={}",
"ddgi": "https://duckduckgo.com/?q={}&iar=images",
"lutris": "https://lutris.net/games/?q={}",
"deal": "https://isthereanydeal.com/search/?q={}",
"gog": "https://www.gog.com/games?sort=popularity&search={}&page=1",
"proton": "https://www.protondb.com/search?q={}",
"qwant": "https://www.qwant.com/?q={}",
"sp": "https://www.startpage.com/do/dsearch?query={}",
"humble": "https://www.humblebundle.com/store/search?sort=bestselling&search={}",
"tor": "https://www.magnetdl.com/search/?m=1&q={}",
"torrent": "https://www.magnetdl.com/search/?m=1&q={}",
"tom": "https://www.rottentomatoes.com/search?search={}",
"u": "http://www.urbandictionary.com/define.php?term={}",
"y": "https://www.youtube.com/results?search_query={}",
"r": "https://old.reddit.com/r/{}/new/",
"gt": "https://translate.google.com/#view=home&op=translate&sl=auto&tl=en&text={}",
"itch": "https://itch.io/search?q={}"}
