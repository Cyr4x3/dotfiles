" init.vim
" cyr4x3 - (c) GNU GPL 2021
" main neovim config file.
" split into several files which are later sourced here


" basic settings
source $HOME/.config/nvim/general/settings.vim
source $HOME/.config/nvim/general/auto_cmd.vim
source $HOME/.config/nvim/general/statusline.vim
source $HOME/.config/nvim/general/netrw.vim
source $HOME/.config/nvim/keys/mappings.vim
source $HOME/.config/nvim/vim-plug/plugins.vim

" plugin config
source $HOME/.config/nvim/plug-config/deoplete.vim
source $HOME/.config/nvim/plug-config/gitgutter.vim
source $HOME/.config/nvim/plug-config/nerdcommenter.vim
source $HOME/.config/nvim/plug-config/ultisnips.vim
