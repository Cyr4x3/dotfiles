" auto-install vim-plug
if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
endif

let g:plug_retries = 0
let g:plug_window = "tabnew"

" plugin list
call plug#begin('~/.config/nvim/autoload/plugged')

    " git
    Plug 'airblade/vim-gitgutter'
    Plug 'tpope/vim-fugitive'

    " latex
    Plug 'sirver/ultisnips'

    " syntax and coloring
    Plug 'sheerun/vim-polyglot'                         " better syntax support
    Plug 'ap/vim-css-color'                             " color previews for css

    " utils
    Plug 'jiangmiao/auto-pairs'                         " auto pairs for '(' '[' '{'
    Plug 'preservim/nerdcommenter'                      " commenter
    Plug 'freitass/todo.txt-vim'

    Plug 'shougo/deoplete.nvim', has('nvim') ? {} : { 'do': [ ':UpdateRemotePlugins']}

call plug#end()
