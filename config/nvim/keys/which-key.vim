" Map leader to which_key
nnoremap <silent> <leader> :silent WhichKey ','<CR>
vnoremap <silent> <leader> :silent <c-u> :silent WhichKeyVisual ','<CR>

" Create map to add keys to
let g:which_key_map =  {}
" Define a separator
let g:which_key_sep = '-' " '→'
" set timeoutlen=100

let g:which_key_display_names = {'<CR>': '↵', '<TAB>': '⇆'}

" No floating window
let g:which_key_use_floating_win = 0
let g:which_key_max_size = 0

" Change the colors if you want
highlight default link WhichKey          Operator
highlight default link WhichKeySeperator DiffAdded
highlight default link WhichKeyGroup     Identifier
highlight default link WhichKeyDesc      Function

" Hide status line
autocmd! FileType which_key
autocmd  FileType which_key set laststatus=0 noshowmode noruler
  \| autocmd BufLeave <buffer> set laststatus=2 noshowmode ruler

" Single mappings
let g:which_key_map['.'] = [ ':e $MYVIMRC'                , 'open init.vim' ]
let g:which_key_map['='] = [ '<C-W>='                     , 'balance windows' ]
let g:which_key_map['<SPC>'] = [ ':e<Space><C-d>'         , 'open file' ]
let g:which_key_map['c'] = [ '<Plug>NERDCommenterToggle'  , 'comment' ]
let g:which_key_map['h'] = [ '<C-W>s'                     , 'split below']
let g:which_key_map['sp'] = [ ':setlocal spell! spell?'   , 'toggle spellcheck' ]
let g:which_key_map['u'] = [ '<leader>u'                  , 'urlview' ]
let g:which_key_map['v'] = [ '<C-W>v'                     , 'split right']

" a is for actions
let g:which_key_map.a = {
      \ 'name' : '+actions' ,
      \ 'h' : [':let @/ = ""'            , 'remove search highlight'],
      \ 'n' : [':set nonumber!'          , 'line-numbers'],
      \ 's' : [':s/\%V\(.*\)\%V/"\1"/'   , 'surround'],
      \ 'r' : [':set norelativenumber!'  , 'relative line nums'],
      \ 'w' : [':set wrap'               , 'wrap on'],
      \ 'W' : [':set nowrap'             , 'wrap off'],
      \ }

" b is for buffer
let g:which_key_map.b = {
      \ 'name' : '+buffer' ,
      \ 'f' : ['bfirst'                 , 'first buffer'],
      \ 'l' : ['blast'                  , 'last buffer'],
      \ 'n' : ['bnext'                  , 'next buffer'],
      \ 'p' : ['bprevious'              , 'previous buffer'],
      \ 'b' : [':buffers<CR>:buffer<Space>' , 'list open buffers'],
      \ }

" d is for document
let g:which_key_map.d = {
      \ 'name' : '+doc' ,
      \ 'c' : [ 'w! \| !~/.local/bin/scripts/compiler "<c-r>%"'   , 'compile' ],
      \ 'p' : [ '!~/.local/bin/scripts/opout <c-r>%'          , 'preview' ],
      \ 'k' : [ ':normal! ,dk'          , 'shellcheck' ],
      \ }

" e is for explorer
let g:which_key_map.e = {
      \ 'name' : '+explorer' ,
      \ 'e' : [ ':Lexplore %:p:h'            , 'current file' ],
      \ 'f' : [ ':Lexplore'                  , 'working directory' ],
      \ }

" g is for git
let g:which_key_map.g = {
      \ 'name' : '+git' ,
      \ 'd' : [':Git diff'                         , 'git diff'],
      \ 'f' : [':Gdiffsplit'                       , 'git diff full'],
      \ 'h' : [':GitGutterLineHighlightsToggle'    , 'highlight hunks'],
      \ 'H' : ['<Plug>(GitGutterPreviewHunk)'      , 'preview hunk'],
      \ 'j' : ['<Plug>(GitGutterNextHunk)'         , 'next hunk'],
      \ 'k' : ['<Plug>(GitGutterPrevHunk)'         , 'prev hunk'],
      \ 's' : [':Git'                              , 'git status'],
      \ 't' : [':GitGutterSignsToggle'             , 'toggle signs'],
      \ 'u' : ['<Plug>(GitGutterUndoHunk)'         , 'undo hunk'],
      \ }

" s is for session
let g:which_key_map.s = {
      \ 'name' : '+Session' ,
      \ 'c' : [':SClose'          , 'Close Session']  ,
      \ 'd' : [':SDelete'         , 'Delete Session'] ,
      \ 'l' : [':SLoad'           , 'Load Session']     ,
      \ 'S' : [':SSave'           , 'Save Session']   ,
      \ }

" t is for tabs
let g:which_key_map.t = {
      \ 'name' : '+tabs' ,
      \ 'c' : [':tabclose'                      , 'close'],
      \ 'o' : [':tabonly'                       , 'only'],
      \ 'w' : [':tabnew'                        , 'new'],
      \ }

" T is for terminal
let g:which_key_map.T = {
      \ 'name' : '+terminal' ,
      \ 'v' : [':vnew term://zsh'                           , 'vsplit'],
      \ 'h' : [':split term://zsh'                          , 'hsplit'],
      \ 't' : [':tabnew term://zsh'                         , 'tab'],
      \ }

" w is for wiki
let g:which_key_map.w = {
      \ 'name' : '+wiki' ,
      \ 'w' : ['<Plug>VimwikiIndex'                      , 'index'],
      \ 'i' : ['<Plug>VimwikiDiaryIndex'                 , 'diary index'],
      \ 's' : ['<Plug>VimwikiUISelect'                   , 'ui select'],
      \ }

autocmd VimEnter * call which_key#register(',', "g:which_key_map")
