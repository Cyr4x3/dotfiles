" mappings.vim
" cyr4x3 - (c) GNU GPLv3 2021
" mappings file for neovim


" splits
nmap <Leader>v <C-w>v
nmap <Leader>h <C-w>s
nmap <Leader>= <C-w>=

" tabs
nmap <Leader>tc :tabclose<CR>
nmap <Leader>to :tabonly<CR>
nmap <Leader>tw :tabnew %<CR>
nnoremap <C-n> :tabn<CR>
nnoremap <C-p> :tabp<CR>

" show all buffers
nnoremap <Leader>bb :buffers<CR>:buffer<Space>

" window navigation
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" window resizing
nnoremap <M-j>    :resize -5<CR>
nnoremap <M-k>    :resize +5<CR>
nnoremap <M-l>    :vertical resize -5<CR>
nnoremap <M-h>    :vertical resize +5<CR>

" terms
map <Leader>Tv :vnew \| term<CR>
map <Leader>Th :split \| term<CR>
map <Leader>Tt :tabnew \| term<CR>

" better nav for omnicomplete
inoremap <expr> <c-j> ("\<C-n>")
inoremap <expr> <c-k> ("\<C-p>")

" C-y on top of a word to get it capitallized
inoremap <c-y> <ESC>viwUi
nnoremap <c-y> viwU<Esc>

" keep selection when using indentation
vnoremap < <gv
vnoremap > >gv

" save file as sudo on files that require root permission
cnoremap w!! execute 'silent! write !sudo tee % >/dev/null' <bar> edit!

" compile and preview commands
map <leader>dc :w! \| !~/.local/bin/scripts/compiler "<c-r>%"<CR>
map <leader>dp :!~/.local/bin/scripts/opout <c-r>%<CR><CR>

" check file in shellcheck:
map <leader>dk :!clear && shellcheck -x %<CR>

" view urls
nnoremap <leader>u :w <bar> startinsert <bar> term urlview %<CR>

" replace all is aliased to S.
nnoremap S :%s//g<Left><Left>

" see content in current dir
map <leader><Space> :e<Space><C-d>

" git
map <leader>gd :Git diff<CR>
map <leader>gf :Gdiffsplit<CR>
map <leader>gs :Git<CR>

" toggle spellcheck
nnoremap <silent> <leader>sp :setlocal spell! spell?<CR>

" netrw
nnoremap <leader>ee :Lexplore %:p:h<CR>
nnoremap <Leader>ef :Lexplore<CR>

" edit init.vim
nmap <leader>. :e $MYVIMRC<CR>
