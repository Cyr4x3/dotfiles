set laststatus=2

function! FileType() abort
    if len(&filetype) == 0
        return " text "
    endif

    return printf(" %s ", tolower(&filetype))
endfunction

let g:currentmode={
       \ 'n'      : 'Normal',
       \ 'no'     : 'N·Operator Pending',
       \ 'v'      : 'Visual',
       \ 'V'      : 'V·Line',
       \ "\<C-V>" : 'V·Block',
       \ 's'      : 'Select',
       \ 'S'      : 'S·Line',
       \ "\<C-S>" : 'S·Block',
       \ 'i'      : 'Insert',
       \ 'R'      : 'Replace',
       \ 'Rv'     : 'V·Replace',
       \ 'c'      : 'Command',
       \ 'cv'     : 'Vim Ex',
       \ 'ce'     : 'Ex',
       \ 'r'      : 'Prompt',
       \ 'rm'     : 'More',
       \ 'r?'     : 'Confirm',
       \ '!'      : 'Shell',
       \ 't'      : 'Terminal'
       \}

set statusline=
set statusline+=%#User1#%{(g:currentmode[mode()]=='Normal')?'\ \ ■\ ':''}
set statusline+=%#User1#%{(g:currentmode[mode()]=='Command')?'\ \ ■\ ':''}
set statusline+=%#User1#%{(g:currentmode[mode()]=='Finder')?'\ \ ■\ ':''}
set statusline+=%#User1#%{(g:currentmode[mode()]=='Terminal')?'\ \ ■\ ':''}
set statusline+=%#User2#%{(g:currentmode[mode()]=='Replace')?'\ \ ■\ ':''}
set statusline+=%#User2#%{(g:currentmode[mode()]=='V·Replace')?'\ \ ■\ ':''}
set statusline+=%#User3#%{(g:currentmode[mode()]=='Insert')?'\ \ ■\ ':''}
set statusline+=%#User4#%{(g:currentmode[mode()]=='Visual')?'\ \ ■\ ':''}
set statusline+=%#User4#%{(g:currentmode[mode()]=='V·Line')?'\ \ ■\ ':''}
set statusline+=%#User4#%{(g:currentmode[mode()]=='V·Block')?'\ \ ■\ ':''}
set statusline+=\%1*\ %t
set statusline+=\ %r
set statusline+=\ %m
set statusline+=\ %l:%c\ \ %p%%
set statusline+=%=
set statusline+=\ %{&fileencoding?&fileencoding:&encoding}
set statusline+=\ \ %{&fileformat}
set statusline+=\ %3*%{FileType()}

hi User1 ctermfg=240 ctermbg=NONE
hi User2 ctermfg=1   ctermbg=NONE
hi User3 ctermfg=2   ctermbg=NONE
hi User4 ctermfg=3   ctermbg=NONE
