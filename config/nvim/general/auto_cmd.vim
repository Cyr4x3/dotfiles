" auto source when writing to init.vim alternatively you can run :source
" $MYVIMRC
au! BufWritePost $MYVIMRC source %

" automatically delete all trailing whitespace and newlines at end of file on
" save and reset cursor position.
au BufWritePre * let currPos = getpos(".")
au BufWritePre * %s/\s\+$//e
au BufWritePre * %s/\n\+\%$//e
au BufWritePre *.[ch] %s/\%$/\r/e
au BufWritePre * cal cursor(currPos[1], currPos[2])

" when shortcut files are updated, renew bash and ranger configs with new
" material:
au BufWritePost bm-files,bm-dirs !shortcuts

" terminal
au BufEnter *
\ if &buftype == "terminal" |
\ startinsert |
\ endif
au TermClose * silent! bd!  " close terminal buffer immediately

" xresources
au BufRead,BufNewFile Xresources,Xdefaults,xresources,xdefaults set filetype=xdefaults
au BufRead,BufNewFile ~/.config/x11/colors/* set filetype=xdefaults
au BufWritePost .Xresources,Xresources,Xdefaults,xresources,xdefaults !xrdb %

" fontconfig
au BufRead,BufNewFile ~/.config/fontconfig/*.conf set filetype=xml

" tex files
au BufRead,BufNewFile *.tex set filetype=tex wrap
au VimLeave *.tex !texclear %

" markdown and gemini files
au BufRead,BufNewFile *.md,*.markdown,*.mdown,*.Rmd,*.rmd set filetype=markdown tw=80
au BufRead,BufNewFile *.gmi set filetype=markdown wrap

" fortran
au BufRead,BufNewFile *.f90 set colorcolumn=132
au BufRead,BufNewFile *.f set colorcolumn=72        " fortran77

" python
au FileType python set colorcolumn=79

au VimEnter * call ViewTips()

" Display a tips
fu! ViewTips()
    " Note that this require the 'fortune' program to be installed on
    " your system, as well as the 'vimtweets' fortune file.
    "   $ wget http://bfontaine.net/fortunes/vimtweets
    "   $ strfile vimtweets vimtweets.dat
    "   # mv vimtweets* /usr/share/games/fortunes/
    " fortune paths may change depending on the distro
    if filereadable('/usr/games/fortune')
        if filereadable('/usr/share/games/fortunes/vimtweets')
            echomsg system('/usr/games/fortune vimtweets')
        endif
    endif
endfu
