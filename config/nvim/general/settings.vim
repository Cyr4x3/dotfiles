let g:mapleader = ","                   " set leader key
let g:maplocalleader = " "              " set local leader key

set hidden                              " hide buffers, not close them.
set encoding=utf-8                      " encoding displayed
set fileencoding=utf-8                  " encoding written to file
set iskeyword+=-                        " treat dash separated words as a word text object
set mouse=                              " disable mouse
set virtualedit=block                   " make visual blocks easier to understand
set conceallevel=0                      " show `` in markdown files and quotes in json files etc.
set updatetime=300                      " faster completion
set timeoutlen=500                      " default timeoutlen is 1000 ms
set formatoptions-=cro                  " stop newline continution of comments
set clipboard=unnamedplus               " copy paste between vim and everything else
set history=1000
set autochdir                           " change $pwd to current file location
set shortmess+=c                        " don't give ins-completion-menu messages

" appearance
syntax enable                           " enable syntax highlighing
colorscheme alduin_mod
set background=dark                     " background color
set t_Co=256                            " support 256 colors
set number                              " line numbers
set relativenumber                      " relative line number
set ruler                               " show the cursor position all the time
set cursorline                          " enable highlighting of the current line
set showtabline=1                       " show tabs only when more than one tab is open
set noshowmode                          " do not show mode in statusbar
set noshowcmd
set laststatus=0                        " always display the status line
set pumheight=10                        " makes popup menu smaller
set scrolloff=7
set sidescrolloff=7

" wrapping
set nowrap                              " display long lines as just one line
set linebreak                           " in case of wrapping, do not split words
set breakindent                         " set indents when wrapped

" indenting
set smartindent                         " smart indent (also toggle autoident on)
set autoindent
set smarttab                            " makes tabbing smarter will realize you have 2 vs 4
set expandtab                           " convert tabs to spaces
set shiftwidth=4                        " number of space characters inserted for indentation
set tabstop=4                           " insert 4 spaces for a tab

" no folding
set foldlevel=99
set foldminlines=99

" maintain undo history between sessions
set undofile
set undodir=~/.config/nvim/undo
set noswapfile

" disable backups
set nobackup
set nowritebackup

" case insensitive search
set ignorecase
set smartcase
set infercase
set incsearch
set nohlsearch                          " don't highlight search results

" completion
set path+=**                            " provides tab-completion for all file related tasks
set omnifunc=syntaxcomplete#Complete    " enable omni completion

" commandline completion
set wildmenu

" ignore files vim doesnt use
set wildignore+=.git,.hg,.svn
set wildignore+=*.aux,*.out,*.toc
set wildignore+=*.o,*.obj,*.exe,*.dll,*.manifest,*.rbc,*.class
set wildignore+=*.ai,*.bmp,*.gif,*.ico,*.jpg,*.jpeg,*.png,*.psd,*.webp
set wildignore+=*.avi,*.divx,*.mp4,*.webm,*.mov,*.m2ts,*.mkv,*.vob,*.mpg,*.mpeg
set wildignore+=*.mp3,*.oga,*.ogg,*.wav,*.flac
set wildignore+=*.eot,*.otf,*.ttf,*.woff
set wildignore+=*.doc,*.pdf,*.cbr,*.cbz
set wildignore+=*.zip,*.tar.gz,*.tar.bz2,*.rar,*.tar.xz,*.kgb
set wildignore+=*.swp,.lock,.DS_Store,._*
set wildignore+=*.pyc,*/.git/*

" show invisibles
set list lcs=tab:·\ ,trail:·,extends:»,precedes:«,nbsp:░

" splits
set fillchars=vert:│,fold:-,stlnc:\ ,stl:─
set splitbelow                          " open splits below
set splitright                          " open vsplits on the right

" spelling
let g:spellfile_URL = 'http://ftp.vim.org/vim/runtime/spell'
