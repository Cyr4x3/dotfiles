let g:vimwiki_list = [{'path':'~/Documents/notes/vimwiki',
        \ 'syntax': 'markdown',
        \ 'ext': '.md'}]

let g:vimwiki_ext2syntax = {'.md': 'markdown',
        \ '.markdown': 'markdown',
        \ '.mdown': 'markdown',
        \ '.Rmd': 'markdown',
        \ '.rmd': 'markdown'}

let g:vimwiki_global_ext = 0
