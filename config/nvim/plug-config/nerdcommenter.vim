let g:NERDCreateDefaultMappings = 0       " Create default mappings
let g:NERDToggleCheckAllLines = 1         " Enable NERDCommenterToggle to check all selected lines is commented or not
let g:NERDTrimTrailingWhitespace = 1      " Enable trimming of trailing whitespace when uncommenting
let g:NERDSpaceDelims = 1                 " Add spaces after comment delimiters by default
let g:NERDCompactSexyComs = 1             " Use compact syntax for prettified multi-line comments

nmap <leader>c <plug>NERDCommenterToggle
vmap <leader>c <plug>NERDCommenterToggle
