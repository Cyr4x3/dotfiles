let g:gitgutter_sign_allow_clobber = 1
let g:gitgutter_sign_added              = '+'
let g:gitgutter_sign_modified           = '!'
let g:gitgutter_sign_removed            = '-'
let g:gitgutter_sign_removed_first_line = '-'
let g:gitgutter_sign_modified_removed   = '-!'
let g:gitgutter_preview_win_floating = 1

let g:gitgutter_enabled = 1

highlight GitGutterAdd    guifg=#90a959 ctermfg=2
highlight GitGutterChange guifg=#d28445 ctermfg=3
highlight GitGutterDelete guifg=#ac4142 ctermfg=1

" Custom mappings
nnoremap <leader>gh :GitGutterLineHighlightsToggle<Enter>
nnoremap <leader>gt :GitGutterSignsToggle<Enter>
nnoremap <leader>gH <plug>GitGutterPreviewHunk
nnoremap <leader>gj <plug>GitGutterNextHunk
nnoremap <leader>gk <plug>GitGutterPrevHunk
nnoremap <leader>gs <plug>GitGutterStageHunk
nnoremap <leader>gu <plug>GitGutterUndoHunk
