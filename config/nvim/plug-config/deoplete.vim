" Use deoplete.
let g:deoplete#enable_at_startup = 1
call deoplete#custom#option({
      \ 'smart_case': v:true,
      \ 'enable_ignore_case': v:false,
      \ })
