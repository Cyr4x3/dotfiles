# bashrc
# cyr4x3 - (c) GNU GPLv3 2021
# Custom configuration file for bash.


# If not running interactively, don't do anything.
case $- in
    *i*) ;;
      *) return ;;
esac

# If existent, load shell shortcuts and aliases.
[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/shell/shortcutrc" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/shell/shortcutrc"
[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/shell/aliasrc" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/shell/aliasrc"

# Shell options.
shopt -s cdspell                  # Autocorrect cd misspellings.
shopt -s cmdhist                  # Save multi-line commands in history as single line.
shopt -s dotglob
shopt -s histappend               # Do not overwrite history, but append it.
shopt -s expand_aliases           # Expand aliases.
shopt -s checkwinsize             # Check term size when bash regains control.

# History settings.
HISTCONTROL=ignoreboth            # Do not keep duplicate lines nor lines starting with space.
HISTSIZE=1000
HISTFILESIZE=2000

# Ignore upper and lowercase letters when TAB completing.
bind "set completion-ignore-case on"

# Enable programmable completion features.
if ! shopt -oq posix; then
    if [ -f /usr/share/bash-completion/bash_completion ]; then
        . /usr/share/bash-completion/bash_completion
    elif [ -f /etc/bash_completion ]; then
        . /etc/bash_completion
    fi
fi

# Set a custom prompt.
_pwd() {
    p="$PWD"
    [ "$p" = "$HOME" ] && p="~"
    printf "$(basename "$p")"
}
PS1='\[\033[35m\]$(_pwd) \[\033[1;37m\]% \[\033[0m\]'
PS2='> '

# Set custom keybindings.
_fzf_history() {
    history | awk '{$1="";print}' | sort -u | fzf --height=25 | xclip -selection clipboard
}
bind -m emacs-standard -x '"\C-r": _fzf_history'
bind -m vi-command -x '"\C-r": _fzf_history'
bind -m vi-insert -x '"\C-r": _fzf_history'
