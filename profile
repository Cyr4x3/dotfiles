#!/bin/sh
#
# profile
# cyr4x3 - (c) GNU GPLv3 2021
# zsh profile file
# runs on login. environmental variables are set here.
#

# Default programs.
export EDITOR="nvim"
export VISUAL="nvim"
export READER="zathura"
export TERMINAL="urxvtc"
export BROWSER="firefox"
export VIDEO="mpv"
export IMAGE="sxiv"
export COLORTERM="truecolor"
export OPENER="xdg-open"
export PAGER="less"

# Build flags.
CORES="$(nproc || echo 1)"
export MAKEFLAGS="-j$CORES"
export CFLAGS="-march=native -O3"
export CXXFLAGS="-march=native -O3"

# XDG folders.
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_STATE_HOME="$HOME/.local/state"

# $HOME cleanup.
export CALENDAR_DIR="${XDG_CONFIG_HOME:-$HOME/.config}/calendar"
export CUDA_CACHE_PATH="${XDG_CACHE_HOME:-$HOME/.cache}/nv"
export DIALOGRC="${XDG_CONFIG_HOME:-$HOME/.config}/dialogrc"
export GTK2_RC_FILES="${XDG_CONFIG_HOME:-$HOME/.config}/gtk-2.0/gtkrc"
export HISTFILE="${XDG_DATA_HOME:-$HOME/.local/share}/history"
export ICEAUTHORITY="${XDG_CACHE_HOME:-$HOME/.cache}/ICEauthority"
export INPUTRC="${XDG_CONFIG_HOME:-$HOME/.config}/shell/inputrc"
export _JAVA_OPTIONS="-Djava.util.prefs.userRoot=${XDG_CONFIG_HOME:-$HOME/.config}/java"
export LESSHISTFILE="-"
export MBSYNCRC="${XDG_CONFIG_HOME:-$HOME/.config}/mbsync/config"
export NOTMUCH_CONFIG="${XDG_CONFIG_HOME:-$HOME/.config}/notmuch-config"
export NPM_CONFIG_USERCONFIG="${XDG_CONFIG_HOME:-$HOME/.config}/npm/npmrc"
export PASSWORD_STORE_DIR="${XDG_DATA_HOME:-$HOME/.local/share}/password-store"
export RCRC="${XDG_CONFIG_HOME:-$HOME/.config}/rcm/rcrc"
export RXVT_SOCKET="$XDG_RUNTIME_DIR/urxvtd"
export STACK_ROOT="${XDG_DATA_HOME:-$HOME/.local/share}/stack"
export TEXMFHOME="${XDG_DATA_HOME:-$HOME/.local/share}/texmf"
export TEXMFVAR="${XDG_CACHE_HOME:-$HOME/.cache}/texlive/texmf-var"
export TEXMFCONFIG="${XDG_CONFIG_HOME:-$HOME/.config}/texlive/texmf-config"
export URXVT_PERL_LIB="${XDG_DATA_HOME:-$HOME/.local/share}/urxvt/ext"
export WEECHAT_HOME="${XDG_CONFIG_HOME:-$HOME/.config}/weechat"
export WGETRC="${XDG_CONFIG_HOME:-$HOME/.config}/wgetrc"
export WINEPREFIX="${XDG_DATA_HOME:-$HOME/.local/share}/wineprefixes/default"
export XINITRC="${XDG_CONFIG_HOME:-$HOME/.config}/x11/xinitrc"
export XAUTHORITY="$XDG_RUNTIME_DIR/Xauthority" # This line will break some DMs.

# Program settings.
export FZF_DEFAULT_OPTS='--tac --layout=reverse --margin=3% --color=16 --cycle -e'
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'
export HISTSIZE=10240
export LESS="-Ri"
export LESSCHARSET=utf-8
export LESSOPEN="| /usr/bin/highlight -O ansi %s 2>/dev/null"
export LESS_TERMCAP_mb=$'\e[01;31m'
export LESS_TERMCAP_md=$'\e[01;34m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[38;33;246m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[01;35m'
export MANWIDTH=80
export NNN_ARCHIVE="\\.(7z|a|ace|alz|arc|arj|bz|bz2|cab|cpio|deb|gz|jar|lha|lz|lzh|lzma|lzo|rar|rpm|rz|t7z|tar|tbz|tbz2|tgz|tlz|txz|tZ|tzo|war|xpi|xz|Z|zip)$"
export NNN_BMS='d:~/Documents;p:~/Pictures;D:~/Downloads/;s:~/Pictures/screenshots;S:~/Documents/Scripts;w:~/Pictures/wallpapers;v:~/Videos;m:~/Music;c:~/Cloud'
export NNN_COLORS='4123'
export NNN_OPTS='edC'
export QT_QPA_PLATFORMTHEME="gtk2"      # Have QT use gtk2 theme.
export SUDO_ASKPASS="$HOME/.local/bin/scripts/dmenu/dmenupass"
export _JAVA_AWT_WM_NONREPARENTING=1    # Fix for Java applications
export TS_SLOTS=5
## ls
export LS_COLORS='no=00;37:fi=00:di=00;34:ln=04;36:pi=40;33:so=01;35:bd=40;33;01'
eval "$(dircolors ~/.config/dircolors)"

# Locales.
export LANG=es_ES.UTF-8
export LANGUAGE=es_ES.UTF-8
export LC_ADDRESS=es_ES.UTF-8
export LC_COLLATE=C
export LC_CTYPE=es_ES.UTF-8
export LC_MEASUREMENT=es_ES.UTF-8
export LC_MESSAGES=es_ES.UTF-8
export LC_MONETARY=es_ES.UTF-8
export LC_NUMERIC=C
export LC_PAPER=es_ES.UTF-8
export LC_TIME=es_ES.UTF-8

# Path.
# Add '~/.local/bin' and its subfolders.
export PATH="${PATH}$(find ${XDG_DATA_HOME%/*}/bin -type d -printf ':%p')"

# Start Syncthing if not already running.
if /usr/bin/pgrep syncthing >/dev/null; then
    echo "Syncthing already started!"
else
    echo "Starting Syncthing..."
    syncthing -no-browser 1>/dev/null 2>&1 &
    echo "Access Syncthing at http://127.0.0.1:8384"
fi

# Start graphical server on user's current tty if not already running.
[ "$(tty)" = "/dev/tty1" ] && ! pidof -s Xorg >/dev/null 2>&1 && exec startx "$XINITRC"
