# vim: set ft=fvwm :

# -----------------------------------------------------------------------
# Name:    fvwm2rc (.fvwm/config)
# Author:  Cyr4x3
# Descr.:  FVWM custom config.
# Credits: Based on u/sehnsuchtbsd 's config.
# -----------------------------------------------------------------------

# Remember to set environmental variable
# FVWM_USERDIR="${XDG_CONFIG_HOME:-$HOME/.config}/fvwm"


##  Environmental variables

# Preferred applications
SetEnv fvwm_browser firefox
SetEnv fvwm_terminal urxvtc
SetEnv fvwm_editor "urxvtc -e nvim"
SetEnv fvwm_mail "urxvtc -e neomutt"
SetEnv fvwm_chat "urxvtc -e weechat"
SetEnv fvwm_player "urxvtc -e ncmpcpp"
SetEnv fvwm_filemanager "urxvtc -e nnn"
SetEnv fvwm_image_viewer sxiv
SetEnv fvwm_runner dmenu


##  General settings

# Ignore CapsLock and NumLock modifiers
IgnoreModifiers 2L

# Resize and move windows instead of
# borders
#OpaqueMoveSize unlimited
#Style * ResizeOpaque

# Indexed window names to make sure
# each window name is unique
Style * IndexedWindowName

# Hides the position and resize dialog
HideGeometryWindow


##  Virtual Desktops

# Number of desktops and default
DesktopSize 3x1
Desktop 0 Main
DesktopName 0 /dev/ws1

# Desktop edge thickness and resistance
# for switching easily with mouse
Style EdgeScroll 100 100
Style EdgeResistance 400 400
Style EdgeThickness 1



##  Auto started applications

# This configuration uses some fvwm modules
# they are also started here
DestroyFunc StartFunction
AddToFunc StartFunction

# FvwmCommandS is used to issue fvwm commands
+ I Module FvwmCommandS

# FvwmAnimate handles some simple animation
+ I Module FvwmAnimate

# FvwmButtons is a powerful tool for creating
# launchers, buttons, switches and such
+ I Module FvwmButtons

# FvwmBacker is used for desktop backgrounds
+ I Module FvwmBacker

# FvwmPerl is used for parsing perl
+ I Module FvwmPerl

# Some start progs
+ I Exec urxvtc
+ I Exec compton
+ I Exec hashwall -f '#404040' -b '#000000' -s 3


##  Functions

# Toggle 'always on top' using TestRc
DestroyFunc WinOnTop
AddToFunc WinOnTop
+ I ThisWindow (Layer 6) Layer
+ I TestRc (NoMatch) Layer 0 6


# Widescreen formatting
DestroyFunc ReadableWidth
AddToFunc ReadableWidth
+ I Maximize 60
+ I Move 50-50w 50-50w

# Autoshade functions set mimics
# kwin autoshade feature
DestroyFunc ToggleAutoshaded
AddToFunc ToggleAutoshaded
+ I State 0

DestroyFunc AutoShade
AddToFunc AutoShade
+ I ThisWindow (State 0) WindowShade True

DestroyFunc AutoUnshade
AddToFunc AutoUnshade
+ I ThisWindow (State 0) WindowShade False

# This module call actually belongs to startup
# function, but I left it here for readability

Module FvwmAuto 0 -menter "Silent AutoUnshade" "Silent AutoShade"

# This is a smart launcher to launch an
# an application if it is not running.
# Also it raises the application if it's
# running and switches between given
# applications/windows if you have two or
# more of them.
DestroyFunc LaunchRaiseSwitch
AddToFunc LaunchRaiseSwitch
+ I None (*$0*) Exec exec $1
+ I Next (*$0*) Focus
+ I Next (*$0*) Iconify False

Module FvwmAuto 0 -mfocus "Silent Raise"

# A mixture of previous two examples would
# let you group windows so that you can
# switch between them with a hotkey.
# Adding a window to a group by chanding
# it's state
DestroyFunc AddToGroup
AddToFunc AddToGroup

+ I ThisWindow State $0

# And switching between groups amd inside them:
DestroyFunc GroupSwitch
AddToFunc GroupSwitch
+ I Next (State $0) Focus

# Undecorate given window
DestroyFunc UnDecorate
AddToFunc UnDecorate
+ I WindowStyle !Title, !Borders, !Handles

# And decorate it again
DestroyFunc Decorate
AddToFunc Decorate
+ I WindowStyle  Borders, Handles

# File browser menu using $[fvwm_filemanager]
AddToFunc FuncFvwmMenuDirectory
+ I PipeRead \
"fvwm-menu-directory -d '$0' \
-command-t 'Exec exec $[fvwm_filemanager] "%d"' \
-command-f 'Exec exec $[fvwm_filemanager] "%f"'"

# Function for titlebar push focus
# and window menu
DestroyFunc TitleFunction
AddToFunc TitleFunction
+ C Focus
+ D Lower
+ M Move

# Undecorate window and maximize it
DestroyFunc UndecMax
AddToFunc UndecMax
+ I ThisWindow UnDecorate
+ I Schedule 100 Maximize 100 100

# Autoshading a window and putting
# it to top layer
DestroyFunc AutoShadeAndOnTop
AddToFunc AutoShadeAndOnTop
+ I ToggleAutoShade
+ I WinOnTop


##  Focus policy

# Similar to that of openbox
Style * MouseFocus

##  Input device bindings

# Cheatsheat
#
# Contexts:
#
#  R - Root window
#  I - Iconified window
#  F - window decoration corner
#  S - window decoration side
#  T - window decoration title
#  W - active window
#  0-9 - titlebar buttons
#  M - Menu
#  A - any context
#
#  You can combine contexts: FWST etc.
#
#  Modifiers:
#  ctrl - C
#  super - 4
#  alt - 1
#  shift - S
#  caps - L
#  num - 2
#
#  You can combine modifiers: 1S, 4S, etc.

# Switch desktops with arrow keys
Key Left A C1 Scroll -100 0
Key Right A C1 Scroll +100 +0
Key Up A C1 Scroll +0 -100
Key Down A C1 Scroll +0 +100

#Alt Tab
SetEnv DIR Next

AddToFunc FocusRaiseAndStuff
+ I Iconify off
+ I Focus
+ I Raise
+ I WarpToWindow !raise 5 5

AddToFunc SwitchWindow
+ I $[DIR] (CurrentPage, !Iconic, !Sticky) FocusRaiseAndStuff
+ I Deschedule 134000
+ I PipeRead `[ "$[DIR]" == "Prev" ] && \
    echo 'SetEnv NDIR Next' || \
    echo 'SetEnv NDIR Prev'`
+ I Schedule 700 134000 SetEnv DIR $[NDIR]

Key Tab A M  SwitchWindow

# Launch apps/menu & restart/quit
Key Return A C1 Exec exec $[fvwm_terminal]
Key Return A 1  Exec exec dmenu_run
Key r A C1 Restart
Key q A C1 Quit

# Mouse bindings
Mouse 3 R A Menu MainMenu
Mouse 1 R A WindowList Root NoGeometry
Mouse 1 T A TitleFunction
Mouse 1 A 1 Move
Mouse 3 A 1 Resize
Mouse 1 W M Move


##  Menus

# Main Menu
AddToMenu MainMenu "Menu" Title
+ "Terminal"                Exec exec urxvtc
+ "Launcher"                Exec exec dmenu_run -p '>> '
+ "Applications"            Popup Applications
+ ""                        Nop
+ "Window Ops"              Popup Window-Ops
+ "X11 Session"             Popup X11-Session

AddToMenu "Window-Ops"
+ "Move"                    Move
+ "Resize"                  Resize
+ "Shade"                   WindowShade
+ "TileLeft"                TileLeft
+ "TileRight"               TileRight
+ "Stick"                   Stick
+ ""                        Nop
+ "Close"                   Close
+ "Kill"                    Destroy

AddToMenu "X11-Session"
+ "Refresh Screen"          Refresh
+ "Restart WM"              Restart
+ ""                        Nop
+ "Lock Screen"             Exec exec slock
+ "Exit FVWM"               Quit

# Application Menu
AddtoMenu Applications
+ "Editors"                 Popup Editors
+ "File Managers"           Popup FM
+ "Terminal Emulators"      Popup Terminal
+ "Network"                 Popup Network
+ "Multimedia"              Popup Multimedia
+ "Graphics"                Popup Graphics
+ "Office"                  Popup Office
+ "System"                  Popup System
+ "Utils"                   Popup Utils

AddtoMenu Editors
+ "Nano"                    Exec exec urxvtc -title Nano -e nano
+ "Nvim"                    Exec exec urxvtc -title NVim -e nvim
+ "XEdit"                   Exec exec xedit

AddtoMenu Network
+ "Browsers"                Popup Browsers
+ "Chat"                    Popup Chat
+ "Neomutt"                 Exec exec urxvtc -title mutt -e neomutt
+ "Newsboat"                Exec exec urxvtc -title Newsboat -e newsboat
+ "Transmission"            Exec exec transmission-gtk

AddtoMenu Browsers
+ "W3m"                     Exec exec urxvtc -g 92x35 \
			  -e w3m -F -graph www.duckduckgo.com
+ "Firefox"                 Exec exec firefox
+ "Qutebrowser"             Exec exec qutebrowser
+ "Amfora"                  Exec exec urxvtc -title Amfora -e amfora
+ "Gopher"                  Exec exec urxvtc -g 92x35 \
			  -e gopher gopher://gopher.floodgap.com/1/v2

AddtoMenu Chat
+ "WeeChat"                 Exec exec urxvtc -title WeeChat -e weechat
+ ""                        Nop
+ "Profanity"               Exec exec urxvtc -title profanity -e profanity
+ ""                        Nop
+ "SDF"                     Exec exec urxvtc -title SDF -e ssh sehnsucht@tty.sdf.org
+ "rtv"                     Exec exec urxvtc -title rtv -e rtv

AddtoMenu FM
+ "nnn"                     Exec exec urxvtc -title nnn -e nnn -edC
+ "PCManFM"                 Exec exec pcmanfm

AddtoMenu Terminal
+ "XTerm"                   Exec exec xterm
+ "urxvtc"                   Exec exec urxvtc

AddtoMenu Multimedia
+ "MPV"                     Exec exec mpv -player-operation-mode=pseudo-gui
+ ""                        Nop
+ "ncmpcpp"                 Exec exec urxvtc  -title ncmpcpp -e ncmpcpp
+ ""                        Nop
+ "Reaper"                Exec exec reaper
+ "Picard"                  Exec exec picard

AddtoMenu Graphics
+ "GColor"                  Exec exec gcolor2
+ "OpenOffice Draw"         Exec exec openoffice-4.1.6-sdraw
+ ""                        Nop
+ "GIMP"                    Exec exec gimp
+ "InkScape"                Exec exec inkscape

AddtoMenu Office
+ "Zathura"                 Exec exec zathura
+ "FBReader"                Exec exec FBReader
+ ""                        Nop
+ "Ghostwriter"             Exec exec gv
+ "XDrawChem"               Exec exec xdrawchem
+ ""                        Nop
+ "OpenOffice Calc"         Exec exec openoffice-4.1.6-scalc
+ "Openoffice Impress"      Exec exec openoffice-4.1.6-simpress
+ "Openoffice Math"         Exec exec openoffice-4.1.6-smath
+ "OpenOffice Writer"       Exec exec openoffice-4.1.6-swriter

AddtoMenu Utils
+ "XCalc"                   Exec exec xcalc
+ "XClock"                  Exec exec xclock
+ "XDiary"                  Exec exec xdiary
+ "XFontSel"                Exec exec xfontsel
+ "XClipboard"              Exec exec xclipboard
+ "Xarchiver"               Exec exec xarchiver
+ "XMan"                    Exec exec xman -notopbox
+ "Abook"                   Exec exec urxvtc -title abook -e abook
+ "Calcurse"                Exec exec urxvtc -title calcurse -e calcurse
+ "Xev"                     Exec exec xev

AddtoMenu System
+ "Settings"  		          Popup Settings
+ ""                 	      Nop
+ "Xosview"                 Exec exec xosview
+ "SMART"                   Exec exec gsmartcontrol
+ "slurm"                   Exec exec urxvtc -e slurm -c -i em0
+ "TCPview"                 Exec exec tcpview
+ "ncdu"                    Exec exec urxvtc -title ncdu -e ncdu
+ "XDiskUsage"              Exec exec xdiskusage
+ "DDD"                     Exec exec ddd
+ "XKill"                   Exec exec xkill
+ ""                 	      Nop
+ "Htop"                    Exec exec urxvtc -title htop -e htop
+ "Top"                     Exec exec urxvtc -title top -e top

AddtoMenu Settings
+ "LXAppearance"            Exec exec lxappearance
+ "LXRandR"                 Exec exec lxrandr
+ "LXInput" 	              Exec exec lxinput
+ "Preferred Applications"  Exec exec exo-preferred-applications
+ "Pulsemixer" 		          Exec exec urxvtc -e pulsemixer
+ "XScreenSaver"            Exec exec xscreensaver-demo

# Window menu
DestroyMenu WindowMenu
AddToMenu WindowMenu
+ "On &Top" WinOnTop
+ "Audo&shade" ToggleAutoShaded
+ "Undecorate and &Maximize" UndecMax
+ "Autoshade and &On Top" AutoShadeAndOnTop
+ "S&tick" Stick
+ "" Nop
+ "Force close" Destroy


## Decorations

# Fonts
MenuStyle "*" Font "xft:Terminus:size=9"

# Cheatsheet:
#
# Colorsets:
#
#  0 = Default colors
#  1 = Inactive windows
#  2 = Active windows
#  3 = Inactive menu entry and menu background
#  4 = Active menu entry
#  5 = greyed out menu entry (only bg used)
#  6 = module foreground and background
#  7 = hilight colors

# Colorsets

# Window Decor CS
Colorset  4 fg #151515, bg  #151515
Colorset  5 fg #151515, bg  #414141, sh #151515, hi #151515
Colorset  10 fg #B5E15D, bg  #151515, sh #555555, hi #555555
Colorset  11 fg #D2B48C, bg  #151515, sh #D2B48C, hi #D2B48C

# Menu  CS
Colorset  7 fg #C7C7C7, bg #303030, sh #414141, hi #414141
Colorset  8 fg #FFFFB9, bg #eb928e, sh #eb928e, hi #151515

# Menu
MenuStyle * MenuColorset 7
MenuStyle * ActiveColorset 8
# 3D
MenuStyle * SeparatorsLong, TrianglesRelief
MenuStyle * BorderWidth 7
MenuStyle * ActiveFore, Hilight3DOff,


# Popup settings
MenuStyle * PopupAsSubmenu, HoldSubmenus, SubmenusRight
MenuStyle "*" PopupOffset 2 100

# De-iconify an iconified window
Mouse 1 I N Iconify

# Misc. Styles
Style FvwmConsole  !Handles, !Icon, WindowListSkip
Style gmrun !Title
Style fvwm-logout

#====================================
#---- Windows
#====================================
# # General settings
 TitleStyle Height 19
 Style * TitleAtTop
 TitleStyle Colorset 11 -- Flat


 # Titlebar colorsets
 Style "*" HilightColorset 11
 Style "*" Colorset 10

ButtonStyle All -- UseTitleStyle
ButtonStyle All Active -- Flat
ButtonStyle All Inactive -- Flat

AddButtonStyle 1 Vector 4 \
60x20@0 60x40@0 80x40@1 80x60@0 60x60@0 60x80@0 40x80@0 40x60@1 20x60@0 20x40@1 40x40@1 40x20@1 60x20@1

AddButtonStyle 2 Vector 5 \
50x45@3 55x40@3 60x45@3  \
55x50@4 55x65@3

AddButtonStyle 4 Vector 4 \
50x50@3 60x60@3 60x50@4 50x60@3

AddButtonStyle 6 Vector 5 \
50x60@3 55x65@3 60x60@3  \
55x55@4 55x40@3

+ TitleStyle -- Flat
+ BorderStyle Simple -- NoInset Flat
+ ButtonStyle All -- UseTitleStyle

 # Titlebar Functions
 Mouse 1 2 N Maximize 100 100
 Mouse 1 4 N Delete
 Mouse 1 6 N Iconify
 Mouse 1 1 A Menu WindowMenu

BorderStyle Active Colorset 10 -- flat
BorderStyle Inactive Colorset 10 -- flat

Style "*" UseDecor MyDecor
Style "*" Font "xft:Terminus:size=9"
Style "*" BorderWidth 2, HandleWidth 2
Style "cli-clock" !Borders, Sticky
Style "acme" !Title
Style "/usr/local/bin/cli-clock" !Borders, Sticky
Style "clock" !Borders, Sticky
Style "FvwmButtons" Sticky, !Title
Style "*" TitleFormat

###Bootys
#&
###Buttons
*FvwmButtonsGeometry 225x98-8-8
*FvwmButtonsBack #151515
*FvwmButtons(Frame 0 Padding 2 2 Container(Rows 2 Columns 2 Frame 0 \
                                           Padding 10 0))
*FvwmButtons(1x2 Frame 0 Swallow(UseOld) "FvwmPager" "Module FvwmPager 0 0 ")
*FvwmPager: Font "xft:Terminus:size=9"
*FvwmPager: Back "#151515"
*FvwmPager: Fore "#b5b19e"
*FvwmPager: Hilight "#151515"
*FvwmPager: WindowColors fore "#404040" back "#b5b19e" hiFore "#151515" hiBack "#151515"
*FvwmPager: SolidSeparators
*FvwmPager: WindowBorderWidth 1
*FvwmButtons(1x2 Frame 0 Container(Rows 2 Columns 2 Frame 0))
*FvwmButtons(Frame 0 Swallow(UseOld,NoHints,Respawn) "xbiff" `Exec exec xbiff -bg "#151515" -fg "#9C9A94"`)
*FvwmButtons(Frame 0 Swallow(UseOld,NoHints,Respawn) "xclock" `Exec exec xclock -bg "#151515" -fg "#b5b19e" -padding 2 -update 1 -d -twentyfour -brief -face "terminus:size=9"`)
*FvwmButtons(2x1 Frame 0 Swallow(UseOld,NoHints,Respawn) "xload" `Exec exec xload -bg "#151515" -fg "#B5b19e" -update 5 -nolabel`)
*FvwmButtons(End)
*FvwmButtons(End)
