#!/bin/sh

notify-send "$(printf 'Installed pkgs: ' && dpkg -l | grep -c ^i && printf 'Updates available: ' && apt list --upgradable 2>/dev/null | tail -n+2 | wc -l)" --icon=none
