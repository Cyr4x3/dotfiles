#!/bin/bash

notify-send "$(sensors | awk '/^temp1/ {print "CPU TEMP:  " $2}' && echo " " && ps axch -o cmd:24,%cpu --sort=-%cpu | head)" --icon=none
