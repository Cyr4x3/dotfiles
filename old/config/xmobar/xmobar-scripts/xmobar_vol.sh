#!/bin/sh

case $(amixer get Master | tail -n1 | awk '{print $NF}' | tr -d '[]') in
	off)
    echo "-"
  ;;
	on)
    VOL=$(amixer get Master |grep % |awk 'FNR==1 {print $5}' |sed 's/[^0-9\%]//g')
    echo "$VOL"
  ;;
esac
