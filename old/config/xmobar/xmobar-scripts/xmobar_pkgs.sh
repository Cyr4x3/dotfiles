#!/bin/sh

num_pkgs=$(dpkg -l | wc -l)
num_upd=$(apt list --upgradable 2>/dev/null | tail -n+2 | wc -l)
echo "$num_upd"
