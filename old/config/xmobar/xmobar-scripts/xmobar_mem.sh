#!/bin/bash

notify-send "$(free -h | awk '/Mem:/ {print "MEM:  " $3 "/" $2}' && echo " " && ps axch -o cmd:24,%mem --sort=-%mem | head)" --icon=none
