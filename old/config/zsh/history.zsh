# zsh history config
# cyr4x3 - (c) GNU GPLv3 2021
# custom history settings file for zsh
# meant to be sourced in the main zshrc file


HISTFILE=~/.cache/zsh/history
HISTSIZE=10000
SAVEHIST=10000

setopt APPEND_HISTORY
setopt EXTENDED_HISTORY                 # Add timestamps
setopt HIST_EXPIRE_DUPS_FIRST           # Remove older duplicates first
setopt HIST_FIND_NO_DUPS                # Do not show command duplicates
setopt HIST_IGNORE_ALL_DUPS             # Do not store duplicates
setopt HIST_REDUCE_BLANKS               # Remove blank spaces
setopt SHARE_HISTORY                    # Shared history between sessions
