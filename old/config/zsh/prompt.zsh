# zsh prompt config
# cyr4x3 - (c) GNU GPLv3 2021
# custom prompt settings file for zsh
# meant to be sourced in the main zshrc file


COLOR_ROOT="%F{red}"
COLOR_USER="%F{cyan}"
COLOR_NORMAL="%F{white}"
PROMPT_STYLE="uoou"
PS2="> "

# allow functions in the prompt
setopt PROMPT_SUBST
autoload -Uz colors && colors

# colors for permissions
if [[ "$EUID" -ne "0" ]]
then  # if user is not root
	USER_LEVEL="${COLOR_USER}"
else # root!
	USER_LEVEL="${COLOR_ROOT}"
fi

# general prompt
case "$PROMPT_STYLE" in
	luke)
		PROMPT='%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%}$%b %f'
	;;
	uoou)
		PROMPT='%B%F{cyan}${vcs_info_msg_0_}%F{magenta}%1~%F{white} %# %f%b'
	;;
	mini)
		PROMPT='${USER_LEVEL}[${COLOR_NORMAL}%~${USER_LEVEL}]── - %f'
	;;
	tiny)
		# PROMPT='%F{3} %%${COLOR_NORMAL} '
		PROMPT='%F{3} ──── ─${COLOR_NORMAL} '
		RPROMPT='%F{15}%~ %F{8}${HOSTNAME}${COLOR_NORMAL}'
	;;
	*)
		PROMPT='${USER_LEVEL}[${COLOR_NORMAL}%~${USER_LEVEL}]── ─ %f'
	;;
esac
