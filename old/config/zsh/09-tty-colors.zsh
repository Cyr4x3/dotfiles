# zsh tty colors
# cyr4x3 - (c) GNU GPLv3 2021
# set tty colors when login into zsh in a tty
# meant to be sourced in the main zshrc file


if [ "$TERM" = "linux" ]
then
    echo -en "\e]P0000000" #black
    echo -en "\e]P84a3637" #darkgrey
    echo -en "\e]P1af5f5f" #darkred
    echo -en "\e]P9af5f5f" #red
    echo -en "\e]P287875f" #darkgreen
    echo -en "\e]PA87875f" #green
    echo -en "\e]P3af875f" #yellow
    echo -en "\e]PBaf875f" #orange
    echo -en "\e]P4769999" #darkblue
    echo -en "\e]PC769999" #blue
    echo -en "\e]P5af8787" #darkmagenta
    echo -en "\e]PDaf8787" #magenta
    echo -en "\e]P657875f" #darkcyan
    echo -en "\e]PE57875f" #cyan
    echo -en "\e]P7bfbf96" #lightgrey
    echo -en "\e]PFbfbf96" #white
    # clear #for background artifacting
    ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=5"
fi
