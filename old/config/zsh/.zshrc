# zshrc
# cyr4x3 - (c) GNU GPLv3 2021
# custom configuration file for zsh


# ---------------------------------------------------------
# BASIC OPTIONS
# ---------------------------------------------------------

# load configs
for config (~/.config/zsh/*.zsh) source $config

# load shortcuts if existent
[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/shell/aliasrc" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/shell/aliasrc"
[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/shell/shortcutrc" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/shell/shortcutrc"
[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/shell/zshnameddirrc" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/shell/zshnameddirrc"

# if not running interactively, don't do anything
[[ $- != *i* ]] && return

# enable colors
autoload -U colors && colors

# do not preserve partial lines
unsetopt PROMPT_SP


# ---------------------------------------------------------
# EXTERNAL PROGRAMS
# ---------------------------------------------------------

# ci", ci', ci`, di", etc
autoload -U select-quoted
zle -N select-quoted
for m in visual viopp; do
  for c in {a,i}{\',\",\`}; do
    bindkey -M $m $c select-quoted
  done
done
