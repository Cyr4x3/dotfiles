-- xmonad.hs
-- cyr4x3 - (c) GNU GPLv3 2021
-- custom configuration file for xmonad

-- either place this file in ~/.xmonad/xmonad.hs or define XMONAD_CACHE_DIR,
-- XMONAD_CONFIG_DIR, and XMONAD_DATA_DIR in your shell profile and place this
-- file accordingly.

-------------------------------------------------------------------------------
-- IMPORTS
-------------------------------------------------------------------------------

  -- Base
import XMonad
import System.Exit
import XMonad.Core

  -- Actions
import XMonad.Actions.CycleWS
import XMonad.Actions.GridSelect
import XMonad.Actions.WindowGo (runOrRaise)
import XMonad.Actions.Submap

  -- Data
import Data.List (isPrefixOf, nub)
import Data.Monoid
import Data.Char (isSpace, toUpper)

  -- Hooks
import XMonad.Hooks.ManageDocks (avoidStruts, docksEventHook, docks, manageDocks, ToggleStruts(..))
import XMonad.Hooks.DynamicLog (dynamicLogWithPP, wrap, xmobarPP, xmobarColor, shorten, PP(..))
import XMonad.Hooks.EwmhDesktops(fullscreenEventHook,ewmh)
import XMonad.Hooks.SetWMName
import XMonad.Hooks.ManageHelpers

  -- Layouts
import XMonad.Layout.GridVariants (Grid(Grid))
import XMonad.Layout.ResizableTile
import XMonad.Layout.ThreeColumns
import XMonad.Layout.Tabbed
import XMonad.Layout.Accordion
import XMonad.Layout.Simplest

  -- Layout modifiers
import XMonad.Layout.Gaps
import XMonad.Layout.LayoutModifier
import XMonad.Layout.MultiToggle
import XMonad.Layout.MultiToggle.Instances
import XMonad.Layout.NoBorders
import XMonad.Layout.Spacing
import XMonad.Layout.SubLayouts
import XMonad.Layout.WindowNavigation       -- Required for sublayouts
import XMonad.Layout.Renamed

import XMonad.ManageHook

  -- Utilities
import XMonad.Util.CustomKeys
import XMonad.Util.Run
import XMonad.Util.SpawnOnce

  -- Import qualified
import qualified XMonad.StackSet as W
import qualified Data.Map        as M


-------------------------------------------------------------------------------
-- LOCAL VARIABLES
-------------------------------------------------------------------------------

myTerminal = "urxvtc"
myFont = "xft:Terminus (TTF):pixelsize=12:antialias=true:hinting:true"

myFocusFollowsMouse = False         -- Whether focus follows the mouse pointer.
myClickJustFocuses = False          -- Whether clicking on a window to focus also passes the click to the window

myBorderWidth = 5                   -- Width of the window border in pixels.
myNormalBorderColor = "#222222"     -- Border colors for unfocused windows.
myFocusedBorderColor = "#4C4C4C"    -- Border colors for focused windows.

myModMask = mod4Mask                -- Specify which modkey you want to use.
                                    -- mod1Mask ("left alt"),  mod3Mask ("right alt"), mod4Mask ("windows key").

myWorkspaces = ["1","2","3","4","5","6","7","8","9"]

myTabConfig = def { fontName = myFont
                  , activeColor = myFocusedBorderColor
                  , activeBorderColor = myFocusedBorderColor
                  , activeTextColor = "#000000"
                  , inactiveColor = "#000000"
                  , inactiveBorderColor = "#000000"
                  , inactiveTextColor = myFocusedBorderColor
                  }


-------------------------------------------------------------------------------
-- GRID SELECT
-------------------------------------------------------------------------------

myNavigation :: TwoD a (Maybe a)
myNavigation = makeXEventhandler $ shadowWithKeymap navKeyMap navDefaultHandler
    where navKeyMap = M.fromList [
                    ((0,xK_Escape), cancel)
                   ,((0,xK_Return), select)
                   ,((0,xK_slash) , substringSearch myNavigation)
                   ,((0,xK_Left)  , move (-1,0)  >> myNavigation)
                   ,((0,xK_h)     , move (-1,0)  >> myNavigation)
                   ,((0,xK_Right) , move (1,0)   >> myNavigation)
                   ,((0,xK_l)     , move (1,0)   >> myNavigation)
                   ,((0,xK_Down)  , move (0,1)   >> myNavigation)
                   ,((0,xK_j)     , move (0,1)   >> myNavigation)
                   ,((0,xK_Up)    , move (0,-1)  >> myNavigation)
                   ,((0,xK_k)     , move (0,-1)  >> myNavigation)
                   ,((0,xK_y)     , move (-1,-1) >> myNavigation)
                   ,((0,xK_i)     , move (1,-1)  >> myNavigation)
                   ,((0,xK_n)     , move (-1,1)  >> myNavigation)
                   ,((0,xK_m)     , move (1,-1)  >> myNavigation)
                   ,((0,xK_space) , setPos (0,0) >> myNavigation)
                   ]
                 -- The navigation handler ignores unknown key symbols
          navDefaultHandler = const myNavigation

spawnSelected' :: [(String, String)] -> X ()
spawnSelected' lst = gridselect conf lst >>= flip whenJust spawn
    where conf = def
                   { gs_cellheight   = 35
                   , gs_cellwidth    = 150
                   , gs_cellpadding  = 10
                   , gs_originFractX = 0.5
                   , gs_originFractY = 0.5
                   , gs_font         = myFont
                   , gs_navigate     = myNavigation
                   }

myAppGrid = [("Wallpaper", "~/.local/bin/scripts/choosebg")
                 , ("LibreOffice", "libreoffice")
                 , ("Torrent", myTerminal ++ " -e rtorrent")
                 , ("Steam", "steam")
                 , ("PCManFM", "pcmanfm")
                 ]


-------------------------------------------------------------------------------
-- KEY-BINDINGS
-------------------------------------------------------------------------------

myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $

  -- Applications
    [ ((modm .|. mod1Mask, xK_Return), spawn myTerminal)                                              -- Launch a terminal
    , ((modm .|. mod1Mask, xK_g), runOrRaise "gimp" (className =? "gimp"))                            -- Launch gimp
    , ((modm .|. mod1Mask, xK_l), runOrRaise "lutris" (className =? "lutris"))                        -- Launch lutris
    , ((modm, xK_s), submap . M.fromList $
      [ ((0, xK_s), spawn "screenkey")                                                                -- Launch screenkey
      , ((0, xK_k), spawn "killall screenkey")                                                        -- Kill all screenkey processes
      ])

  -- Grid Select
    , ((modm .|. shiftMask, xK_p), spawnSelected' myAppGrid)                          -- Grid select favorite apps

  -- Layouts
    , ((modm,               xK_space), sendMessage NextLayout)                        -- Rotate through the available layout algorithms
    , ((modm .|. shiftMask, xK_Mode_switch), setLayout $ XMonad.layoutHook conf)      -- Reset the layouts on the current workspace to default
    , ((modm .|. shiftMask, xK_space), sendMessage ToggleStruts)                      -- Toggle Struts
    , ((modm .|. shiftMask, xK_n), sendMessage $ Toggle FULL)                         -- Toggle noborder

  -- Windows
    , ((modm, xK_n), refresh)                                                         -- Resize viewed windows to the correct size
    , ((modm, xK_j), windows W.focusDown)                                             -- Move focus to the next window
    , ((modm, xK_k), windows W.focusUp)                                               -- Move focus to the previous window
    , ((mod1Mask, xK_Tab), windows W.focusUp)                                         -- Move focus to the previous window
    , ((mod1Mask .|. shiftMask, xK_Tab), windows W.focusDown)                         -- Move focus to the next window
    , ((modm, xK_h), windows W.focusMaster)                                           -- Move focus to the master window
    , ((modm, xK_t),                                                                  -- Push window into tiling if not floating - float if tiling
          withFocused (\windowId -> do { floats <- gets (W.floating . windowset);
          if windowId `M.member`floats
          then withFocused $ windows. W.sink
          else float windowId }))
    , ((modm, xK_comma ), sendMessage (IncMasterN 1))                                 -- Increment the number of windows in the master area
    , ((modm, xK_period), sendMessage (IncMasterN (-1)))                              -- Deincrement the number of windows in the master area
    , ((modm .|. controlMask, xK_h), sendMessage Shrink)                              -- Shrink the master area
    , ((modm .|. controlMask, xK_l), sendMessage Expand)                              -- Expand the master area
    , ((modm .|. controlMask, xK_j), sendMessage MirrorShrink)                        -- Shrink the master area vertically
    , ((modm .|. controlMask, xK_k), sendMessage MirrorExpand)                        -- Expand the master area vertically
    , ((modm .|. shiftMask, xK_g), sequence_[sendMessage ToggleGaps
                                  , toggleScreenSpacingEnabled
                                  , toggleWindowSpacingEnabled])                      -- Toggle Gaps and Window and Screen spacing
    , ((modm .|. shiftMask, xK_c), kill)                                              -- Close focused window
    , ((modm .|. shiftMask, xK_x), spawn "xkill")                                     -- Launch xkill
    , ((modm .|. shiftMask, xK_h), windows W.swapMaster)                              -- Swap the focused window and the master window
    , ((modm .|. shiftMask, xK_j), windows W.swapDown)                                -- Swap the focused window with the next window
    , ((modm .|. shiftMask, xK_k), windows W.swapUp  )                                -- Swap the focused window with the previous window
    , ((modm, xK_a), submap . M.fromList $
      [ ((0, xK_h), sendMessage $ pullGroup L)
      , ((0, xK_j), sendMessage $ pullGroup D)
      , ((0, xK_k), sendMessage $ pullGroup U)
      , ((0, xK_l), sendMessage $ pullGroup R)
      , ((0, xK_a), withFocused (sendMessage . MergeAll))
      , ((0, xK_u), withFocused (sendMessage . UnMergeAll))
      , ((0, xK_t), withFocused (sendMessage . UnMerge))
      , ((0, xK_y), toSubl NextLayout)
      ])
    , ((modm .|. shiftMask, xK_period), onGroup W.focusUp')                           -- Switch focus to next tab
    , ((modm .|. shiftMask, xK_comma), onGroup W.focusDown')                          -- Switch focus to prev tab

  -- Multimedia
    -- Headphones buttons
    , ((0, 0x1008ff11), spawn "amixer -q sset Master 5%-")          -- XF86AudioLowerVolume
    , ((0, 0x1008ff13), spawn "amixer -q sset Master 5%+")          -- XF86AudioRaiseVolume

  -- XMonad
    , ((modm .|. shiftMask, xK_apostrophe), spawn (myTerminal ++ " -e ~/.local/bin/scripts/xmonad_keys"))     -- Show keybindings
    , ((modm, xK_q), spawn "xmonad --recompile; xmonad --restart")                                            -- Restart xmonad
    -- , ((modm .|. shiftMask, xK_q     ), io (exitWith ExitSuccess))                                            -- Quit xmonad
    ]
    ++

  -- Workspaces
    [((m .|. modm, k), windows $ f i)                                           -- mod-[1..9], Switch to workspace N
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]                 -- mod-shift-[1..9], Move client to workspace N
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
    ++
    [((m .|. modm, key), screenWorkspace sc >>= flip whenJust (windows . f))    -- mod-{w,e,r}, Switch to physical/Xinerama screens 1, 2, or 3
        | (key, sc) <- zip [xK_w, xK_e, xK_r] [0..]                             -- mod-shift-{w,e,r}, Move client to screen 1, 2, or 3
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]
    ++
    [((modm, xK_0), toggleWS)]                                                  -- Go to previous workspace


-------------------------------------------------------------------------------
-- MOUSE BINDINGS
-------------------------------------------------------------------------------

myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))

    , ((mod1Mask, button3), (\w -> spawn "~/.local/bin/scripts/pmenu-launch"))
    -- you may also bind events to the mouse scroll wheel (button4 and button5)
    ]


-------------------------------------------------------------------------------
-- LAYOUTS
-------------------------------------------------------------------------------

-- You can specify and transform your layouts by modifying these values.
-- If you change layout bindings be sure to use 'mod-shift-space' after
-- restarting (with 'mod-q') to reset your layout state to the new
-- defaults, as xmonad preserves your old layout settings by default.

myLayout = id
    . smartBorders
    . mkToggle (NOBORDERS ?? FULL ?? EOT)
    $ avoidStruts (tall ||| grid ||| threecol ||| tab)
  where
     -- default tiling algorithm partitions the screen into two panes

     tall   = renamed [Replace "m"]
              $ addTabs shrinkText myTabConfig
              $ subLayout [] (Simplest ||| Accordion)
              $ configurableNavigation noNavigateBorders
              $ subTabbed
              $ gaps [(U,5), (D,5), (R,5), (L,5)]
              $ spacing 5
              $ ResizableTall 1 (3/100) (1/2) []

     grid  = renamed [Replace "g"]
              $ addTabs shrinkText myTabConfig
              $ subLayout [] (Simplest ||| Accordion)
              $ configurableNavigation noNavigateBorders
              $ subTabbed
              $ gaps [(U,5), (D,5), (R,5), (L,5)]
              $ spacing 5
              $ Grid (16/10)

     threecol = renamed [Replace "tc"]
              $ addTabs shrinkText myTabConfig
              $ subLayout [] (Simplest ||| Accordion)
              $ configurableNavigation noNavigateBorders
              $ subTabbed
              $ gaps [(U,5), (D,5), (R,5), (L,5)]
              $ spacing 5
              $ ThreeColMid 1 (3/100) (1/2)

     tab = renamed [Replace "t"]
              $ tabbed shrinkText myTabConfig


-------------------------------------------------------------------------------
-- WINDOW RULES
-------------------------------------------------------------------------------

myManageHook = composeAll . concat $
    [ [resource  =? r --> doIgnore | r <- myIgnores]
    , [className =? c --> doFloat | c <- myFloats]
    , [className =? c --> doCenterFloat | c <- myCenterFloats]
    , [isDialog --> doCenterFloat]
    ]
      where
        myFloats = ["MPlayer", "Screenkey"]
        myIgnores = ["desktop_window", "kdesktop"]
        myCenterFloats = ["Gcr-prompter", "Gcolor2", "MEGAsync", "XCalc", "Yad"]


-------------------------------------------------------------------------------
-- EVENT HANDLING
-------------------------------------------------------------------------------

-- * EwmhDesktops users should change this to ewmhDesktopsEventHook
-- Defines a custom handler function for X Events. The function should
-- return (All True) if the default handler is to be run afterwards. To
-- combine event hooks use mappend or mconcat from Data.Monoid.

myEventHook = mempty


-------------------------------------------------------------------------------
-- STATUS BARS AND LOGGING
-------------------------------------------------------------------------------

-- Perform an arbitrary action on each internal state change or X event.
-- See the 'XMonad.Hooks.DynamicLog' extension for examples.

myLogHook = return ()


-------------------------------------------------------------------------------
-- STARTUP HOOK
-------------------------------------------------------------------------------

-- Perform an arbitrary action each time xmonad starts or is restarted
-- with mod-q.  Used by, e.g., XMonad.Layout.PerWorkspace to initialize
-- per-workspace layout choices.
-- By default, do nothing.

myStartupHook = do
        setWMName "LG3D" -- Used to avoid problems when running certain programs (e.g. JDownloader)

-------------------------------------------------------------------------------
-- MAIN
-------------------------------------------------------------------------------

-- Now run xmonad with all the defaults we set up.
-- Run xmonad with the settings you specify. No need to modify this.

main = do
    xmproc <- spawnPipe "xmobar -x 0 ~/.config/xmobar/xmobarrc"
    xmonad $ docks $ ewmh $ defaults
      { manageHook = myManageHook <+> manageDocks
      , logHook = myLogHook <+> dynamicLogWithPP xmobarPP
                        { ppOutput = \x -> hPutStrLn xmproc x
                        , ppCurrent = xmobarColor "#999686" "" . wrap "[" "]"   -- Current workspace in xmobar
                        , ppVisible = xmobarColor "#999686" ""                  -- Visible but not current workspace
                        , ppHidden = xmobarColor "#695F5A" "" . wrap "*" ""     -- Hidden workspaces in xmobar
                        , ppHiddenNoWindows = xmobarColor "#403A37" ""          -- Hidden workspaces (no windows)
                        , ppTitle = xmobarColor "#695F5A" "" . shorten 60       -- Title of active window in xmobar
                        -- , ppSep =  "<fc=#666666> <fn=2>|</fn> </fc>"            -- Separators in xmobar
                        , ppUrgent = xmobarColor "#C45500" "" . wrap "!" "!"    -- Urgent workspace
                        , ppOrder  = \(ws:l:t:ex) -> [ws,l]++ex++[t]            -- Show Workspaces + Layout + Windowtitle
                        -- , ppOrder  = \(ws:_:t:_) -> [ws,t]                      -- Show Workspaces + Windowtitle
                        }
      }

-- A structure containing your configuration settings, overriding
-- fields in the default config. Any you don't override, will
-- use the defaults defined in xmonad/XMonad/Config.hs
-- No need to modify this.

defaults = def {
      -- simple stuff
        terminal           = myTerminal,
        focusFollowsMouse  = myFocusFollowsMouse,
        clickJustFocuses   = myClickJustFocuses,
        borderWidth        = myBorderWidth,
        modMask            = myModMask,
        workspaces         = myWorkspaces,
        normalBorderColor  = myNormalBorderColor,
        focusedBorderColor = myFocusedBorderColor,

      -- key bindings
        keys               = myKeys,
        mouseBindings      = myMouseBindings,

      -- hooks, layouts
        layoutHook         = myLayout,
        manageHook         = myManageHook,
        handleEventHook    = myEventHook,
        logHook            = myLogHook,
        startupHook        = myStartupHook
    }
