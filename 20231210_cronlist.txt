*/15 * * * * env DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/$(id -u $USER)/bus /usr/bin/mpc update
*/15 * * * * env DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/$(id -u $USER)/bus /home/ricard/.local/bin/cron/newsup
*/3 * * * * env DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/$(id -u $USER)/bus /home/ricard/.local/bin/cron/mailsync >/dev/null 2>&1
*/2 * * * * env DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/$(id -u $USER)/bus /home/ricard/.local/bin/cron/maillist
